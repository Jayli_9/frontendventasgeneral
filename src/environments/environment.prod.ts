export const environment = {
  production: true,
  hmr: false,
  prefijoApp:"ACD",
  serviciosBackend:'https://desacademico.educacion.gob.ec/seguridades-servicios',
  firebaseConfig:{
    //...
  },
  url_seguridades: 'https://desacademico.educacion.gob.ec/seguridades-servicios',
  url_academico:'https://desacademico.educacion.gob.ec/academico-servicios',
  url_catalogo:'https://desacademico.educacion.gob.ec/catalogo-servicios',
  url_oferta:'https://desacademico.educacion.gob.ec/Oferta-servicios',
  url_docente:'https://desacademico.educacion.gob.ec/docentesApp-servicios',
  urlDocentes: 'https://desacademico.educacion.gob.ec/docentes-servicios',
  url_institucion:'https://desacademico.educacion.gob.ec/giee-servicios',
  url_matricula:'https://desacademico.educacion.gob.ec/matricula-servicios',
  url_prueba: 'http://127.0.0.1:8080/academico-servicios'
};
