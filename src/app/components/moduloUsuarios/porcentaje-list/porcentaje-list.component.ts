import { Component,HostBinding, OnInit } from '@angular/core';
import { NotificacionService } from '../../../services/notificacion.service';

import { PorcentajesService } from '../../../services/porcentajes.service';
import { Porcentaje } from '../../../models/cat_porcentaje';
@Component({
  selector: 'app-porcentaje-list',
  templateUrl: './porcentaje-list.component.html',
  styleUrls: ['./porcentaje-list.component.css']
})
export class PorcentajeListComponent implements OnInit {

  @HostBinding('class') classes = 'row';
  porcentajes: any = [];
  selectedPorcentaje: Porcentaje;

  constructor(private porcentajeService: PorcentajesService, 
    private notificacion:NotificacionService) { }

  ngOnInit() {

    this.getPorcentajes();
    
  }

  getPorcentajes(){
    this.porcentajeService.getPorcentajes().subscribe(
      res => {
        this.porcentajes = res
      },
      err => console.error(err)
    );
  }

  deletePorcentaje(id: number)
  {
    this.porcentajeService.deletePorcentaje(id).subscribe(
      res => {
        setTimeout(()=>{
          this.notificacion.showInfo('El porcentaje se ha eliminado','Estado civil eliminado');

        },200);
        this.getPorcentajes()
      },
      err => console.error(err)
    );

  }
}
