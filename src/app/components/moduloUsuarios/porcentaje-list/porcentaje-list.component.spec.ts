import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PorcentajeListComponent } from './porcentaje-list.component';

describe('PorcentajeListComponent', () => {
  let component: PorcentajeListComponent;
  let fixture: ComponentFixture<PorcentajeListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PorcentajeListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PorcentajeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
