import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PorcentajeFormComponent } from './porcentaje-form.component';

describe('PorcentajeFormComponent', () => {
  let component: PorcentajeFormComponent;
  let fixture: ComponentFixture<PorcentajeFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PorcentajeFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PorcentajeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
