import { Component, HostBinding, OnInit } from '@angular/core';
import { Porcentaje } from '../../../models/cat_porcentaje';
import { PorcentajesService } from '../../../services/porcentajes.service';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificacionService } from '../../../services/notificacion.service';
@Component({
  selector: 'app-porcentaje-form',
  templateUrl: './porcentaje-form.component.html',
  styleUrls: ['./porcentaje-form.component.css']
})
export class PorcentajeFormComponent implements OnInit {
  @HostBinding('class') classes = 'row';

  porcentajes: Porcentaje = {
    nombrePorcentaje:'',
    valorPorcentaje:0
  }
  
  creacion:string='';
  edit : boolean = false;
  constructor(private porcentajeServices: PorcentajesService, 
    private router: Router,
    private activedrouter: ActivatedRoute,
    private notificacion: NotificacionService) { }

  ngOnInit() {
    this.creacion = 'Crear';
    const params = this.activedrouter.snapshot.params;
    if(params.id){
      this.porcentajeServices.getPorcentaje(params.id).subscribe(
        res=>{
          if(res!= null){
            this.creacion = 'Actualizar';
            this.porcentajes = res; 
            this.edit = true;

          }else{
            this.router.navigate(['/admin/percentage']);
          }
          
        },
        err => console.log("hay error "+ err)
      )
    }

  }

  savePorcentaje(){
    
    if(this.testingresar()){
     
      this.porcentajeServices.savePorcentaje(this.porcentajes).subscribe(
        res=>{

          setTimeout(()=>{
            this.notificacion.showSuccess('El porcentaje se ha agregado correctamente','Categoria agregada');
          },100)
          this.router.navigate(['/admin/percentage']);
        },error => console.error(error)
      );
    }else{
      this.notificacion.showError('Revise si todos los campo esten llenos','**Error al agergar Categoria')
    }
  }

  updatePorcentaje(){

    if(this.testingresar()){
      this.porcentajeServices.updatePorcentaje(this.porcentajes.idPorcentajes, this.porcentajes).subscribe(
        res => {
          this.porcentajes.nombrePorcentaje = '';
          this.porcentajes.valorPorcentaje = 0;
          setTimeout(()=>{
            this.notificacion.showSuccess('El porcentaje se ha actualizado correctamente','Categoria actualizada');
          },100)

          this.router.navigate(['/admin/percentage'])
        },
        err => console.error(err)
      );
    }else{
      this.notificacion.showError('Revise si estan llenos los campos','**Error al actuclizar la Categoria')
    }
  }

  

  testingresar() {
    
    if (this.porcentajes.nombrePorcentaje.length !=0 ) {
      return true;
    } else {
      
      return false;
    }
  }
}
