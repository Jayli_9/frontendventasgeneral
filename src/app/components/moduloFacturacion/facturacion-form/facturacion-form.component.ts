import { Component, HostBinding, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificacionService } from '../../../services/notificacion.service';


//stockAuxiliar
import { cat_stockAuxiliar } from '../../../models/cat_stockAuxiliar';
import { CatStockService } from '../../../services/cat-stock.service';



//CLIENTES
import { Clientes } from '../../../models/Clientes';
import { ClientesService } from '../../../services/clientes.service';
import { resolve } from 'url';
import { VenCabezaFactura } from 'src/app/models/VenCabezaFactura';
import { VenDetalleFact } from 'src/app/models/VenDetalleFact';
import { FacturacionService } from '../../../services/facturacion.service';
import { ProductoService } from '../../../services/producto.service';
import { PuntosVentasService } from '../../../services/puntos-ventas.service';
import { ThrowStmt } from '@angular/compiler';
import { cat_stock } from 'src/app/models/cat_stock';
import { Productos } from 'src/app/models/cat_Producto';
import { PuntosVentas } from 'src/app/models/cat_PuntosVenta';
import { Porcentaje } from 'src/app/models/cat_porcentaje';

import { UsuariosService } from '../../../services/usuarios.service';
import { ComprobantesService } from '../../../services/comprobantes.service';
import { modeloFacturaSRI } from '../../../models/modelosSRI/modeloFacturaSRI';
import { totalImpuesto } from '../../../models/modelosSRI/totalImpuesto';
import { CookieService } from 'ngx-cookie-service';
import { waitForAsync } from '@angular/core/testing';
import { map } from 'rxjs/operators';
declare let $: any;

@Component({
  selector: 'app-facturacion-form',
  templateUrl: './facturacion-form.component.html',
  styleUrls: ['./facturacion-form.component.css']
})
export class FacturacionFormComponent implements OnInit {
  @HostBinding('class') classes = 'row';
  //objeto stok busqueda para ventas
  stock: cat_stockAuxiliar = {
    cantidad: 0,
    catProducto: {
      idProductos: 0,
      catCategoria: {
        idCategoria: 0,
        descripcion: '',
        nombreCategoria: '',
      },
      catMedida: {
        idMedidas: 0,
        medida: '',
        abreviacion: '',
        tipo: '',
      },
      codProducto: ''
    },
    catPuntosVenta: { idPuntosVenta: 0 },
    existe: '',
    precioDistribuidor: 0,
    precioMayor: 0,
    precioUnit: 0,
    precioBulto: 0,
    stockMax: 0,
    stockMin: 0
  };

  //////////////////////VARIABLES PARA CREAR FACTURA/////////////////////////////

  totalImpuestoAux: totalImpuesto[];

  modeloFacturaSRI: modeloFacturaSRI = {
    infoTributaria: {},
    infoFactura: {
      totalImpuesto: [{}],
      pago: [{}]
    },
    detalle: [{

      codigoAuxiliar: "",
      codigoPrincipal: "",
      descripcion: "",
      cantidad: 0,
      precioUnitario: 0,
      descuento: 0,
      precioTotalSinImpuesto: 0,

      detAdicional: [{
        nombre: "-",
        valor: "-"
      }],

      impuesto: [{
        codigo: "",
        codigoPorcentaje: "",
        tarifa: 0,
        baseImponible: 0,
        valor: 0
      }]

    }],

    campoAdicional: [{}]
  }
  clienteSRI: Clientes = {};
  puntoDeVentaSRI: PuntosVentas = {};
  productoSRI: Productos = {};
  impuestoSRI: Porcentaje = {};
  stockSRI: cat_stockAuxiliar = {};


  ///////////////////////////////////////////////////



  //////////////////////VARIABLE OBNTENER RESULTADO API FACTURA/////////////////////////
  resultadoApi: any = {
    success: true,
    result: {
      claveAcceso: null
    }
  };

  ////////////////////////////////////////////////

  listafacturaIngreso: VenCabezaFactura[];
  listaDetalleFactura: VenDetalleFact[];
  isloading = false;
  selectedDetalles: VenDetalleFact;
  selectedValue: string = 'val1';
  //variables
  idProductoConsulta: number = 0;
  codigoProducto;
  idPuntosVenta: number;
  detalle: string = "";
  precioUnit: number = 0;
  precioMay: number = 0;
  precioDis: number = 0;
  precioBulto: number = 0;
  cantidad: number = 0;


  subtotalIva12: number = 0;
  subtotalIva0: number = 0;

  subtotalFactura: number = 0;
  descuentoFactura: number = 0;
  ivaFactura: number = 0;
  porcentajeDescuentoSeleccionado: number = 0;
  tipoPago: string = '';

  precioSeleccionado: number = 0;
  fact: boolean = true;
  notaVenta: boolean = false;

  tipofactSeleccionado: string = "true";

  totalIngresoVista: string = "0";
  totalVenta: string = "0";
  totalVentaAxuliar: string = "0";

  totalDescuento: string = '0';

  encuentraArray = false;
  //variables Stock
  cantidadDisponible: number = 0;
  cantidadConsulta: number = 0;
  cantidadLista: number = 0;



  cantidadDisponibleAUx: any = [];
  //variables cliente
  cedula: string = "";
  nombreCliente: string = "";
  apellidoCliente: string = "";
  telefono: string = "";
  email: string = "";
  codigoCedula: string = "";
  direccion: string = "";
  idClienteIngreso: number = 0;
  //objeto cliente
  nuevoClienteIngreso: Clientes = {

    idCliente: 0,
    apellidoCli: "",
    cedulaCli: "",
    direccionCli: "",
    email: "",
    nombreCli: "",
    telefono: "",
    codigo: ""

  };

  clientmodal: Clientes = {

    idCliente: 0,
    apellidoCli: "",
    cedulaCli: "",
    direccionCli: "",
    email: "",
    nombreCli: "",
    telefono: ""

  };

  auxiliarFacturaIngreso: VenCabezaFactura = {
    estado: "",
    tipoPago: "",
    tipoComprobante: "",
    iva: 0,
    fechaFactu: "",
    total: 0,
    subtotal: 0,
    descuento: 0,
    usUser: {
      idUsuario: 0,
    },
    detallefact: [{
      cantidadFact: 0,
      descripcion: "",
      valorTotal: 0,
      valorTotalSinIva: 0,
      valorUnit: 0,
      valorUnitSinIva: 0,
      ivaProducto: 0,
      catStock:
      {
        id: {
          idProductos: 0,
          idPuntosVenta: 0
        }
      }
    }

    ],
    venCliente: {
      idCliente: 0
    }

  }

  //objetto tipo venDetallefactura
  venDetalleFactura: VenDetalleFact = {
    idDetalleFact: 0,
    cantidadFact: 0,
    descripcion: "",
    valorTotal: 0,
    valorTotalSinIva: 0,
    valorUnit: 0,
    valorUnitSinIva: 0,
    ivaProducto: 0,
    catStock: {
      id: {
        idPuntosVenta: 0,
        idProductos: 0
      }
    }
  }
  displayConsultar: boolean = false;
  displayCliente: boolean = false;
  stockConsulta: any = [];
  selectedStock: cat_stock;
  disabled: boolean = false;
  usuarioId: number = 0;

  //variables para consultar el valor de un iva o en la tabla porcentaje

  valorPorcentaje: number = 0;
  idPorcentajeConsulta: number = 0;
  productoConsulta: Productos;



  constructor(private stockService: CatStockService,
    private clienteService: ClientesService,
    private facturaService: FacturacionService,
    private puntosVentaService: PuntosVentasService,
    private comprobantesServicesSRI: ComprobantesService,
    private cookies: CookieService,
    private router: Router,
    private activedrouter: ActivatedRoute,
    private productoService: ProductoService,
    private notificacion: NotificacionService,
    private userService: UsuariosService) {

    activedrouter.params.subscribe(val => {
      this.ngOnInit();
      this.inicializarVariables();
    })
  }


  ngOnInit() {
    this.tipofactSeleccionado = 'true';
    const params = this.activedrouter.snapshot.params;
    this.disabled = false;
    this.idPuntosVenta = Number(params.id);
    //console.log("hola", this.idPuntosVenta)
    this.totalIngresoVista = "0";
    this.listafacturaIngreso = [{
      idCabezaFac: 0,
      estado: "",
      iva: 0,
      fechaFactu: "",
      total: 0,
      subtotal: 0,
      descuento: 0,
      usUser: {
        idUsuario: this.usuarioId,
      },
      detallefact: [{
        idDetalleFact: 0,
        cantidadFact: 0,
        descripcion: "",
        valorTotal: 0,
        valorUnit: 0,
        catStock: {
          id: {
            idPuntosVenta: 0,
            idProductos: 0
          }
        }
      }
      ],
      venCliente: {
        idCliente: 0
      },

    }
    ];

    this.listaDetalleFactura = [];
    this.getStockConsulta(this.idPuntosVenta);
    this.userService.getUserLogged().subscribe(res => {
      // console.log("el usuario logeado es "+res[0].idUsuario);
      this.usuarioId = res[0].idUsuario;
    })
  }



  obtenerVariable(nombreProd: any) {
    this.codigoProducto = nombreProd;
    this.displayConsultar = false;

    this.encontrarProducto(this.codigoProducto);
    (<HTMLInputElement>document.getElementById("primerRadio")).checked = true;
    // (<HTMLInputElement>document.getElementById("primerRadioFact")).checked = true;

  }
  //seleccionar el precio del radio button
  radioChangeHandler($event: any) {
    this.precioSeleccionado = $event.target.value;
  }
  //seleccionar el precio del radio button
  radioChangeHandlerTipoFact($event: any) {
    this.tipofactSeleccionado = $event.target.value;
    console.log(this.tipofactSeleccionado);
  }
  imprimirRadiobutton() {
    //(<HTMLInputElement>document.getElementById("primerRadio")).checked = true;

    //let valorRadiobutton = document.getElementById("primerRadio").checked;

    // console.log(x);
    //console.log(this.precioSeleccionado)

  }

  //Metodo para agregar datos a una lista para posteriormente ingresar a la bdd
  async Agregar() {

    if ((this.cedula.length !== 13 &&
      this.cedula.length !== 10) ||
      this.nombreCliente.length == 0 ||
      this.telefono.length == 0 ||
      this.email.length == 0 ||
      this.direccion.length == 0
    ) {
      this.notificacion.showError('Ingrese datos correctos del cliente', '**Error')


    } else {
      this.disabled = true;
      //para agregar datos al cliente si los campos no estan vacios

      const IDCLIENTE = new Promise(async (resolve, reject) => {
        await this.clienteService.getClienteByCedula(this.cedula).subscribe((res) => {
          if (Object.keys(res).length === 0) {

            resolve(0)
          } else {
            resolve(res[0].idCliente);
          }

        }, err => console.log(err))
      });

      await IDCLIENTE.then(res => {
        //console.log("el cliente existe", res)
        this.idClienteIngreso = Number(res);
      })


      //creamos un nuevo cliente sis que no existe
      if (this.idClienteIngreso == 0) {
        this.nuevoClienteIngreso.nombreCli = this.nombreCliente;
        this.nuevoClienteIngreso.apellidoCli = this.apellidoCliente;
        this.nuevoClienteIngreso.cedulaCli = this.cedula;
        this.nuevoClienteIngreso.direccionCli = this.direccion;
        this.nuevoClienteIngreso.telefono = this.telefono;
        this.nuevoClienteIngreso.email = this.email;
        if (this.cedula.length === 10) {
          this.codigoCedula = "05";
        } else if (this.cedula.length === 13) {
          this.codigoCedula = "04";
        }
        this.nuevoClienteIngreso.codigo = this.codigoCedula;

        //console.log("el nuevo clientea ingresar = > ", this.nuevoClienteIngreso);

        const IDCLIENTEINGREO = new Promise(async (resolve, reject) => {
          await this.clienteService.saveCliente(this.nuevoClienteIngreso).subscribe(res => {
            resolve(res.idCliente);
          }, error => console.log(error))
        });

        await IDCLIENTEINGREO.then(res => {
          this.idClienteIngreso = Number(res);
        })

        //console.log("id CLiente ingresado=>  ", this.idClienteIngreso)
      } else {
        //console.log("el cliente existe", this.idClienteIngreso)
      }


      if (this.codigoProducto === "" || this.codigoProducto === 0) {
        this.notificacion.showError('Ingrese condigo de un producto', '**Error');

      } else {



        if (this.cantidadDisponible < this.cantidad) {
          this.notificacion.showError('No existe cantidad suficiente para realizar la venta', '**Error');



        } else {
          ///INGRESAR DATOS SI EXISTE EL CLIENTE

          this.venDetalleFactura.cantidadFact = this.cantidad;
          this.venDetalleFactura.descripcion = this.detalle;
          this.venDetalleFactura.valorTotalSinIva = Number(this.precioSeleccionado * this.cantidad);
          this.venDetalleFactura.valorUnitSinIva = Number(this.precioSeleccionado);



          // this.venDetalleFactura.valorUnit = Number(this.precioUnit);
          this.venDetalleFactura.catStock.id.idProductos = this.idProductoConsulta;
          this.venDetalleFactura.catStock.id.idPuntosVenta = this.idPuntosVenta;

          //realizar consulta del valor del iva de acuerdo a cada id del producto 
          //y obtener el valor para realizar la respectiva multipicación
          const IDPORCEN = new Promise(async (resolve, reject) => {
            await this.productoService.findbyId(this.venDetalleFactura.catStock.id.idProductos).toPromise().then(res => {
              resolve(res)

            })
          })
          await IDPORCEN.then(res => this.productoConsulta = res);
          //obtenemos el valor del iva para realizar las operaciones 
          this.valorPorcentaje = this.productoConsulta.catPorcentaje.valorPorcentaje;

          //calculamos el valor del iva total y por precio unitario

          this.venDetalleFactura.valorTotal = Number(this.precioSeleccionado * this.cantidad) + (Number(this.precioSeleccionado * this.cantidad) * this.valorPorcentaje);
          this.venDetalleFactura.valorUnit = Number(this.precioSeleccionado) + (Number(this.precioSeleccionado) * this.valorPorcentaje);
          this.venDetalleFactura.ivaProducto = Math.round(Number(this.venDetalleFactura.valorTotalSinIva * this.valorPorcentaje) * 100) / 100;


          // this.ivaFactura = Math.round((totalvista - this.subtotalFactura) * 100) / 100;
          if (this.listaDetalleFactura.length === 0) {

            if (this.venDetalleFactura.cantidadFact <= 0) {
              this.notificacion.showError('Ingrese cantidad mayor a 0', '**Error')


            } else {
              //añadimos el primer elemento a la lista
              this.listaDetalleFactura.push(this.venDetalleFactura);


              this.totalIngresoVista = "" + this.venDetalleFactura.cantidadFact * this.venDetalleFactura.valorUnit;

              this.totalVenta = "" + this.venDetalleFactura.valorTotal;
              this.totalVentaAxuliar = "" + this.venDetalleFactura.valorTotal;
              this.ivaFactura = Math.round((Number(this.precioSeleccionado * this.cantidad) * this.valorPorcentaje) * 100) / 100;
              this.subtotalFactura = this.venDetalleFactura.valorTotalSinIva;

              if (this.valorPorcentaje === 0) {

                this.subtotalIva0 = this.subtotalFactura;
              }
              if (this.valorPorcentaje === 0.12) {

                this.subtotalIva12 = this.subtotalFactura;
              }



              this.cantidadDisponible = this.cantidadDisponible - this.venDetalleFactura.cantidadFact;



              //enviamos una variable falsa si existe el productodespues de ingresar
              this.encuentraArray = false;

            }

          } else {

            if (this.venDetalleFactura.cantidadFact <= 0) {

              this.notificacion.showError('Ingrese cantidad mayor a 0', '**Error')
            } else {
              this.cantidadLista = 0;
              this.cantidadDisponible = 0;

              for (var x in this.listaDetalleFactura) {
                //realizamos la validación para verificar si existe el prodcuto dentro de la lista Stock
                if (this.listaDetalleFactura[x].catStock.id.idProductos == this.venDetalleFactura.catStock.id.idProductos
                  && this.listaDetalleFactura[x].catStock.id.idPuntosVenta == this.venDetalleFactura.catStock.id.idPuntosVenta
                ) {



                  this.listaDetalleFactura[x].valorUnitSinIva = this.precioSeleccionado;
                  // sumatoria de la cantidad de un elemento encontrado
                  this.listaDetalleFactura[x].cantidadFact = Number(this.listaDetalleFactura[x].cantidadFact) + Number(this.venDetalleFactura.cantidadFact);



                  //calculamos el iva
                  this.listaDetalleFactura[x].valorTotalSinIva = Number(this.listaDetalleFactura[x].cantidadFact * Number(this.listaDetalleFactura[x].valorUnitSinIva));

                  this.listaDetalleFactura[x].ivaProducto = Math.round((Number(this.listaDetalleFactura[x].valorTotalSinIva * this.valorPorcentaje)) * 100) / 100;

                  this.listaDetalleFactura[x].valorTotal = this.listaDetalleFactura[x].valorTotalSinIva + this.listaDetalleFactura[x].ivaProducto;


                  //cambiamos la varibnale encuentraArray a tr5u al momento que se encuentra el porducto en el stocklista

                  this.encuentraArray = true;
                  this.cantidadLista = this.listaDetalleFactura[x].cantidadFact;

                  this.cantidadDisponible = this.cantidadConsulta - this.cantidadLista;


                }

              }

              //  se realiza la valicaión si existe el procuto en el array
              if (this.encuentraArray) {
                // reiniciar valores para la nueva busqueda del elemento en el array para el siguiente proceso
                this.encuentraArray = false;
              } else {
                //si no existe el producto ingresa un nuevo elemento en el array 
                //metodo push para apilar elemnto en el array
                this.listaDetalleFactura.push(this.venDetalleFactura);

                this.cantidadDisponible = this.cantidadConsulta - this.venDetalleFactura.cantidadFact;

                this.encuentraArray = false;
              }

              let totalvista = 0;
              this.subtotalFactura = 0;
              this.ivaFactura = 0;
              this.subtotalIva0 = 0;
              this.subtotalIva12 = 0;
              for (var x in this.listaDetalleFactura) {

                if (this.listaDetalleFactura[x].ivaProducto === 0) {
                  this.subtotalIva0 = this.subtotalIva0 + this.listaDetalleFactura[x].valorTotalSinIva;
                }
                if (this.listaDetalleFactura[x].ivaProducto !== 0) {
                  this.subtotalIva12 = this.subtotalIva12 + this.listaDetalleFactura[x].valorTotalSinIva;
                }

                totalvista += (this.listaDetalleFactura[x].valorTotal);

                this.subtotalFactura += this.listaDetalleFactura[x].valorTotalSinIva;
              }


              this.subtotalFactura = Math.round(this.subtotalFactura * 100) / 100;

              this.subtotalIva12 = Math.round(this.subtotalIva12 * 100) / 100;
              this.subtotalIva0 = Math.round(this.subtotalIva0 * 100) / 100;

              this.ivaFactura = Math.round((totalvista - this.subtotalFactura) * 100) / 100;

              this.totalIngresoVista = "" + totalvista;
              this.totalVenta = "" + Math.round((totalvista) * 100) / 100;
              this.totalVentaAxuliar = "" + totalvista;
            }



          }

          //console.log(this.listaDetalleFactura);
          //objetto tipo venDetallefactura
          this.venDetalleFactura = {
            idDetalleFact: 0,
            cantidadFact: 0,
            descripcion: "",
            valorTotal: 0,
            valorTotalSinIva: 0,
            valorUnit: 0,
            valorUnitSinIva: 0,
            ivaProducto: 0,
            catStock: {
              id: {
                idPuntosVenta: 0,
                idProductos: 0
              }
            }

          }
        }

        this.codigoProducto = 0;
        this.buscarStockProducto();
        (<HTMLInputElement>document.getElementById("primerRadio")).checked = true;
      }
    }


  }

  inicializarVariables() {
    this.auxiliarFacturaIngreso={};
    this.idProductoConsulta = 0;
    this.codigoProducto = 0;
    //  (<HTMLInputElement>document.getElementById("primerRadio")).checked = true;
    this.detalle = "";
    this.precioUnit = 0;
    this.precioMay = 0;
    this.precioDis = 0;
    this.precioBulto = 0;
    this.subtotalFactura = 0;
    this.subtotalIva0 = 0;
    this.subtotalIva12 = 0;
    this.descuentoFactura = 0;
    this.ivaFactura = 0;
    this.cantidad = 0;
    this.precioSeleccionado = 0;
    this.totalIngresoVista = "0";
    this.totalVenta = "0";
    this.totalVentaAxuliar = "0";
    this.encuentraArray = false;
    this.cedula = "";
    this.listafacturaIngreso = [];
    this.listaDetalleFactura = [];

    this.cantidadDisponible = 0;



    this.nombreCliente = "";
    this.apellidoCliente = "";
    this.telefono = "";
    this.email = "";
    this.direccion = "";
    this.idClienteIngreso = 0;

    this.auxiliarFacturaIngreso = {
      estado: "",
      tipoPago: "",
      tipoComprobante: "",
      iva: 0,
      fechaFactu: "",
      total: 0,
      subtotal: 0,
      descuento: 0,
      usUser: {
        idUsuario: 0
      },
      detallefact: [{
        cantidadFact: 0,
        descripcion: "",
        valorTotal: 0,
        valorTotalSinIva: 0,
        valorUnit: 0,
        valorUnitSinIva: 0,
        ivaProducto: 0,
        catStock:
        {
          id: {
            idProductos: 0,
            idPuntosVenta: 0
          }
        }
      }

      ],
      venCliente: {
        idCliente: 0
      }

    }


  }
  async Vender() {


    this.disabled = false;
    if (this.tipoPago === "") {

      this.notificacion.showError('Seleccione tipo de pago', '**Error')
    } else {


      let idfacturaPDF = 0;
      let fecha = new Date()
      let fechaFormateada = fecha.getFullYear() + "-" + ("0" + (fecha.getMonth() + 1)).slice(-2) + "-" + ("0" + (fecha.getDate())).slice(-2);
      //console.log(fechaFormateada);
      this.auxiliarFacturaIngreso.fechaFactu = fechaFormateada;
      this.auxiliarFacturaIngreso.estado = "A";
      this.auxiliarFacturaIngreso.tipoPago = this.tipoPago;
      this.auxiliarFacturaIngreso.iva = this.porcentajeDescuentoSeleccionado;
      this.auxiliarFacturaIngreso.total = (Number(this.totalVenta));
      this.auxiliarFacturaIngreso.subtotal = (Number(this.subtotalFactura));
      this.auxiliarFacturaIngreso.descuento = (Number(this.descuentoFactura));
      this.auxiliarFacturaIngreso.iva = (Number(this.ivaFactura));
      this.auxiliarFacturaIngreso.usUser.idUsuario = this.usuarioId;//Usuario logeaado 
      this.auxiliarFacturaIngreso.venCliente.idCliente = this.idClienteIngreso;





      if (this.listaDetalleFactura.length === 0) {
        this.notificacion.showError('No hay datos para realizar la venta', '**Error')
        // this.isloading=true;
      } else {

        this.isloading = true;


        for (let i = 0; i < this.listaDetalleFactura.length; i++) {
          this.auxiliarFacturaIngreso.detallefact[i] = {

            cantidadFact: Number(this.listaDetalleFactura[i].cantidadFact),
            descripcion: this.listaDetalleFactura[i].descripcion,
            valorTotal: Number(this.listaDetalleFactura[i].valorTotal),
            valorTotalSinIva: Number(this.listaDetalleFactura[i].valorTotalSinIva),
            valorUnit: Number(this.listaDetalleFactura[i].valorUnit),
            valorUnitSinIva: Number(this.listaDetalleFactura[i].valorUnitSinIva),
            ivaProducto: Number(this.listaDetalleFactura[i].ivaProducto),
            catStock: {
              id: {
                idPuntosVenta: Number(this.listaDetalleFactura[i].catStock.id.idPuntosVenta),
                idProductos: Number(this.listaDetalleFactura[i].catStock.id.idProductos)
              }
            }




          }
        }
        ////////aqui va el codigo de crear factura SRI para obtener la clave de acceso y guardar en nuestra base de datos
        ///llenar datos en el modelo para crear la factura al SRI



        this.modeloFacturaSRI.id = "comprobante";
        this.modeloFacturaSRI.version = "1.0.0";
        //////info tributaria, o información del local y de la empresa
        this.modeloFacturaSRI.infoTributaria.ambiente = "1";
        this.modeloFacturaSRI.infoTributaria.tipoEmision = "1";
        this.modeloFacturaSRI.infoTributaria.razonSocial = "'Empresa Creaciones F &amp; L'";
        this.modeloFacturaSRI.infoTributaria.nombreComercial = "'Empresa Creaciones F &amp; L'";
        this.modeloFacturaSRI.infoTributaria.ruc = "1002699757001"; //importadora fyl
        // this.modeloFacturaSRI.infoTributaria.ruc = "1003866173001"; kigche
        this.modeloFacturaSRI.infoTributaria.codDoc = "01";

        await this.generarNumeroSecuencialNumeroFactura();

        this.modeloFacturaSRI.infoTributaria.estab = "001";
        this.modeloFacturaSRI.infoTributaria.ptoEmi = this.numeroFacturero;
        this.modeloFacturaSRI.infoTributaria.secuencial = this.secuencial;
        this.modeloFacturaSRI.infoTributaria.dirMatriz = "matriz local";

        /////INFORMACIÓN FACTURA////////////

        //////consultar datos del cliente para la factura

        const CLIENTE = new Promise(async (resolve, reject) => {
          await this.clienteService.getCliente(this.auxiliarFacturaIngreso.venCliente.idCliente).toPromise().then(res => {
            resolve(res)

          })
        })
        await CLIENTE.then(res => this.clienteSRI = res);
        //consultar el objeto punto de venta
        const PUNTOVENTA = new Promise(async (resolve, reject) => {
          await this.puntosVentaService.getPuntosVenta(this.idPuntosVenta).toPromise().then(res => {
            resolve(res)

          })
        })
        await PUNTOVENTA.then(res => this.puntoDeVentaSRI = res);
        //llenamos los datos de la factura
        let fecha = new Date()

        let fechaFormateada1 = ("0" + (fecha.getDate())).slice(-2) + "/" + ("0" + (fecha.getMonth() + 1)).slice(-2) + "/" + fecha.getFullYear();
        //de aqui empieza la infoFactura
        this.modeloFacturaSRI.infoFactura.fechaEmision = fechaFormateada1;
        this.modeloFacturaSRI.infoFactura.dirEstablecimiento = this.puntoDeVentaSRI.direccion; //dirección local de venta
        this.modeloFacturaSRI.infoFactura.contribuyenteEspecial = "000";
        this.modeloFacturaSRI.infoFactura.obligadoContabilidad = "NO";
        this.modeloFacturaSRI.infoFactura.tipoIdentificacionComprador = this.clienteSRI.codigo; //05 es cedula
        this.modeloFacturaSRI.infoFactura.guiaRemision = this.modeloFacturaSRI.infoTributaria.estab + "-" + this.modeloFacturaSRI.infoTributaria.ptoEmi + "-" + this.modeloFacturaSRI.infoTributaria.secuencial; //solucionar validación para luiego enviar la factura
        this.modeloFacturaSRI.infoFactura.razonSocialComprador = this.clienteSRI.apellidoCli + " " + this.clienteSRI.nombreCli;
        this.modeloFacturaSRI.infoFactura.identificacionComprador = this.clienteSRI.cedulaCli;
        this.modeloFacturaSRI.infoFactura.direccionComprador = this.clienteSRI.direccionCli;
        this.modeloFacturaSRI.infoFactura.totalSinImpuestos = this.auxiliarFacturaIngreso.subtotal;
        this.modeloFacturaSRI.infoFactura.totalDescuento = 0;
        this.modeloFacturaSRI.infoFactura.propina = 0;
        this.modeloFacturaSRI.infoFactura.importeTotal = this.auxiliarFacturaIngreso.total; //valor total sumado el iva
        this.modeloFacturaSRI.infoFactura.moneda = "DOLAR";
        //crear el array de las formas de pago en este caso solo una forma de pago
        this.modeloFacturaSRI.infoFactura.pago[0].formaPago = "01";
        this.modeloFacturaSRI.infoFactura.pago[0].total = this.auxiliarFacturaIngreso.total;//total del valor a pagar sumado el iva
        this.modeloFacturaSRI.infoFactura.pago[0].plazo = 1;//numero de dias en la que se paga
        this.modeloFacturaSRI.infoFactura.pago[0].unidadTiempo = "dias";//unidad de tiempo que se va a pagar
        //seguir con los datos de la facturua
        this.modeloFacturaSRI.infoFactura.valorRetIva = 0;
        this.modeloFacturaSRI.infoFactura.valorRetRenta = 0;
        // console.log("hola inicio =>", this.modeloFacturaSRI.detalle);

        /////////////////////////////////////

        // for (let x = 0; x < this.auxiliarFacturaIngreso.detallefact.length; x++) {
        this.totalImpuestoAux = [];
        for (var x in this.auxiliarFacturaIngreso.detallefact) {

          //consultar el stock
          const STOCK = new Promise(async (resolve, reject) => {
            await this.stockService.findbyIdproductoIdpuntosVenta(this.auxiliarFacturaIngreso.detallefact[x].catStock.id.idProductos, this.idPuntosVenta).toPromise().then(res => {
              resolve(res)

            })
          })
          await STOCK.then(res => this.stockSRI = res[0]);

          // console.log(this.stockSRI)


          // console.log("hola antes ", x, "=>", this.modeloFacturaSRI.detalle[x]);
          this.modeloFacturaSRI.detalle[x] = {
            codigoPrincipal: this.stockSRI.catProducto.codProducto,
            codigoAuxiliar: this.stockSRI.catProducto.codProducto,
            descripcion: this.auxiliarFacturaIngreso.detallefact[x].descripcion,
            cantidad: this.auxiliarFacturaIngreso.detallefact[x].cantidadFact,
            precioUnitario: this.auxiliarFacturaIngreso.detallefact[x].valorUnitSinIva,
            descuento: 0,
            precioTotalSinImpuesto: this.auxiliarFacturaIngreso.detallefact[x].valorTotalSinIva,

            detAdicional: [{
              nombre: "-",
              valor: "-",
            }],
            //realizar consulta de los valores del porcentaje de cada productoo


            impuesto: [{
              codigo: "2",
              codigoPorcentaje: this.stockSRI.catProducto.catPorcentaje.codigoPorcentaje,
              tarifa: this.stockSRI.catProducto.catPorcentaje.valorPorcentaje * 100,
              baseImponible: this.auxiliarFacturaIngreso.detallefact[x].valorTotalSinIva,
              valor: this.auxiliarFacturaIngreso.detallefact[x].ivaProducto,
            }]

          }
          let encontreImpuesto = false;
          let codigoPorcentajeAux = this.modeloFacturaSRI.detalle[x].impuesto[0].codigoPorcentaje;
          let codigoAux = this.modeloFacturaSRI.detalle[x].impuesto[0].codigo;
          let valorAux = this.modeloFacturaSRI.detalle[x].impuesto[0].valor;;
          let baseImponibleAux = this.modeloFacturaSRI.detalle[x].impuesto[0].baseImponible;
          let tarifaAux = this.modeloFacturaSRI.detalle[x].impuesto[0].tarifa;

          // console.log("hola ", x, "=>", this.modeloFacturaSRI.detalle[x]);

          if (this.totalImpuestoAux.length === 0) {

            this.totalImpuestoAux.push({
              codigo: codigoAux,
              codigoPorcentaje: codigoPorcentajeAux,
              baseImponible: baseImponibleAux,
              valor: valorAux
            });

            // console.log(this.totalImpuestoAux);
          }
          else {

            // console.log("ya existe un elemento de un impuesto");
            for (var y in this.totalImpuestoAux) {

              // console.log(this.totalImpuestoAux[y].codigoPorcentaje + " es igual a" + codigoPorcentajeAux);

              if (this.totalImpuestoAux[y].codigoPorcentaje === codigoPorcentajeAux) {
                // console.log("existe un elemento igual con ese codigo ");
                let nuevoValorAux = this.totalImpuestoAux[y].valor + valorAux;
                let nuevoBaseimpoibleAux = this.totalImpuestoAux[y].baseImponible + baseImponibleAux;


                this.totalImpuestoAux[y] = {
                  codigo: codigoAux,
                  codigoPorcentaje: codigoPorcentajeAux,
                  baseImponible: nuevoBaseimpoibleAux,
                  valor: Math.round(Number(nuevoValorAux) * 100) / 100
                }
                encontreImpuesto = true;

              }

            }

            if (encontreImpuesto) {

            } else {

              this.totalImpuestoAux.push({
                codigo: codigoAux,
                codigoPorcentaje: codigoPorcentajeAux,
                baseImponible: baseImponibleAux,
                valor: valorAux
              });
              encontreImpuesto = false;
            }

          }



        }


        this.modeloFacturaSRI.infoFactura.totalImpuesto = this.totalImpuestoAux;
        this.modeloFacturaSRI.origen = "OTRO"


        this.modeloFacturaSRI.campoAdicional = [{
          nombre: "email",
          value: "-"
        }]

        // console.log(JSON.stringify(this.modeloFacturaSRI));

        // this.cookies.set("toquen","12712c99-504a-4c4a-9597-7d57264dd514");


        ///realizo el control si se escogio una factura o nota de venta

        if (this.tipofactSeleccionado === "true") {

          // console.log(JSON.stringify(this.modeloFacturaSRI))
          // console.log(this.modeloFacturaSRI);
          //////////////////////////////INICIO CREACIÓN FACTURA EN VERONICA/////////////////////////////////////////

          this.auxiliarFacturaIngreso.tipoComprobante = "factura";

          const obtenerClaveAcceso = new Promise(async (resolve, reject) => {
            this.comprobantesServicesSRI.crearFacturaSRI(this.modeloFacturaSRI).subscribe(
              res => {
                resolve(res)


              }, error => {
                // console.error(error)
                this.isloading = false;
                this.notificacion.showError('No se pudo crear la factura '+error.message, '**Error')
              });
          })
          let a: any;
          a = await obtenerClaveAcceso.then(res => res);

          // console.log("clave de acceso=> " + a);
          this.auxiliarFacturaIngreso.claveAcceso = a;
          this.auxiliarFacturaIngreso.ptoEmision = this.modeloFacturaSRI.infoTributaria.ptoEmi;
          this.auxiliarFacturaIngreso.guiaRemision = this.modeloFacturaSRI.infoFactura.guiaRemision;
          this.auxiliarFacturaIngreso.secuencial = this.modeloFacturaSRI.infoTributaria.secuencial;
          this.auxiliarFacturaIngreso.estab = this.modeloFacturaSRI.infoTributaria.estab;

          //////////////////////////////FIN CREACIÓN FACTURA EN VERONICA/////////////////////////////////////////

          if (this.auxiliarFacturaIngreso.claveAcceso === "") {

            this.notificacion.showError('No se pudo crear la factura', '**Error')
          } else {

            ////////////////////////Enviar y Autorizar la factura en el SRI/////////////////////////////

            const obtenerAutorizacion = new Promise(async (resolve, reject) => {
              this.comprobantesServicesSRI.enviarAutorizarFacturaSRI(this.auxiliarFacturaIngreso.claveAcceso).subscribe(
                res => {
                  resolve(res)


                }, error => {
                  console.error(error)
                  this.isloading = false
                });
            })
            let validacion: any;
            validacion = await obtenerAutorizacion.then(res => res);

            // console.log("mensaje validación=>", validacion)

            ////////////////////////  FIN ===> Enviar y Autorizar la factura en el SRI/////////////////////////////
            if (validacion === "true") {


              /////////////////////Ingresar datos a nuestra base de datos////////////////////////////////
              // console.log(this.auxiliarFacturaIngreso)
              const obtenerid = new Promise(async (resolve, reject) => {
                await this.facturaService.saveFactura(this.auxiliarFacturaIngreso).subscribe(res => {
                  //console.log(res)
                  resolve(res.idCabezaFac)
                }, err => console.log(err))
              })

              idfacturaPDF = await obtenerid.then(res => Number(res));
              //console.log("este es el id de la factura realizada", idfacturaPDF)
              let restaCantidad = 0;
              let cantidadLista = 0;
              let cantidadConsulta = 0;
              for (let i = 0; i < this.listaDetalleFactura.length; i++) {

                const ActualizarStockCantidad = new Promise(async (resolve, reject) => {
                  await this.stockService.findbyIdproductoIdpuntosVenta(this.listaDetalleFactura[i].catStock.id.idProductos, this.listaDetalleFactura[i].catStock.id.idPuntosVenta)
                    .subscribe(res => {

                      restaCantidad = 0;

                      cantidadConsulta = res[0].cantidad;
                      cantidadLista = this.listaDetalleFactura[i].cantidadFact;
                      restaCantidad = cantidadConsulta - cantidadLista;



                      this.stockService.updateStockCantidadRest(Number(restaCantidad), this.listaDetalleFactura[i].catStock.id.idProductos, this.listaDetalleFactura[i].catStock.id.idPuntosVenta)
                        .subscribe(res => {
                          //console.log("si actualizamos")
                          resolve(res);
                        })
                    }, err => console.log(err))

                })
                await ActualizarStockCantidad.then(res => res);

              }
              /////////////////////Fin Ingresar datos a nuestra base de datos////////////////////////////////

              /////////////////////consultar ticket de nuestra base de datos////////////////////////////////
              this.facturaService.ticket(idfacturaPDF).subscribe(res => {
                let pdfWindow = window.open("")
                pdfWindow.document.write(
                  "<iframe width='100%' height='100%' src='data:application/pdf;base64, " +
                  encodeURI(res[0]) + "'></iframe>"
                )
                this.isloading = false;
              },
                err => console.log(err));


              ///////////////////// FIN consultar ticket de nuestra base de datos////////////////////////////////

              this.inicializarVariables();
            } else {
              this.isloading = false;

              this.notificacion.showError('No se puedo generar la factura en el SRI', '**Error')
              /////////////////////Ingresar datos a nuestra base de datos////////////////////////////////
              // console.log(this.auxiliarFacturaIngreso)
              this.auxiliarFacturaIngreso.estado = "ANU";
              const obtenerid = new Promise(async (resolve, reject) => {
                await this.facturaService.saveFactura(this.auxiliarFacturaIngreso).subscribe(res => {
                  //console.log(res)
                  resolve(res.idCabezaFac)
                }, err => console.log(err))
              })

              idfacturaPDF = await obtenerid.then(res => Number(res));
            }
          }
        } else {


          this.auxiliarFacturaIngreso.tipoComprobante = "nota";
          // console.log(this.tipofactSeleccionado);
          /////////////////////Ingresar datos a nuestra base de datos////////////////////////////////
          console.log(this.auxiliarFacturaIngreso)
          const obtenerid = new Promise(async (resolve, reject) => {
            await this.facturaService.saveFactura(this.auxiliarFacturaIngreso).subscribe(res => {
              //console.log(res)
              resolve(res.idCabezaFac)
            }, err => console.log(err))
          })

          idfacturaPDF = await obtenerid.then(res => Number(res));
          //console.log("este es el id de la factura realizada", idfacturaPDF)
          let restaCantidad = 0;
          let cantidadLista = 0;
          let cantidadConsulta = 0;
          for (let i = 0; i < this.listaDetalleFactura.length; i++) {

            const ActualizarStockCantidad = new Promise(async (resolve, reject) => {
              await this.stockService.findbyIdproductoIdpuntosVenta(this.listaDetalleFactura[i].catStock.id.idProductos, this.listaDetalleFactura[i].catStock.id.idPuntosVenta)
                .subscribe(res => {

                  restaCantidad = 0;

                  cantidadConsulta = res[0].cantidad;
                  cantidadLista = this.listaDetalleFactura[i].cantidadFact;
                  restaCantidad = cantidadConsulta - cantidadLista;



                  this.stockService.updateStockCantidadRest(Number(restaCantidad), this.listaDetalleFactura[i].catStock.id.idProductos, this.listaDetalleFactura[i].catStock.id.idPuntosVenta)
                    .subscribe(res => {
                      //console.log("si actualizamos")
                      resolve(res);
                    })
                }, err => console.log(err))

            })
            await ActualizarStockCantidad.then(res => res);

          }
          /////////////////////Fin Ingresar datos a nuestra base de datos////////////////////////////////

          /////////////////////consultar ticket de nuestra base de datos////////////////////////////////
          this.facturaService.ticket(idfacturaPDF).subscribe(res => {
            let pdfWindow = window.open("")
            pdfWindow.document.write(
              "<iframe width='100%' height='100%' src='data:application/pdf;base64, " +
              encodeURI(res[0]) + "'></iframe>"
            )
            this.isloading = false;
          },
            err => console.log(err));


          ///////////////////// FIN consultar ticket de nuestra base de datos////////////////////////////////

        }
        
        this.inicializarVariables();

      }

      // console.log(JSON.stringify(this.modeloFacturaSRI));
      ///////////////////// FIN consultar ticket de nuestra base de datos////////////////////////////////




    }
  }


  encontrarProducto(encontrar: string): void {
    if (encontrar.length == 0) {

    } else {
      this.buscarStockProducto();
    }


  }
  encontrarCliente(cedula: string): void {
    if (cedula.length == 0) {
      this.nombreCliente = "";
      this.apellidoCliente = "";
      this.telefono = "";
      this.email = "";
      this.direccion = "";
    } else {
      this.buscarClienteByCedula();
    }


  }


  ////////////////////////////////////PARA LLENAR AUTOMATICAMENTE LOS VALORES EN LOS CAMPOS////////////////////////////////////////////////
  async buscarStockProducto() {

    this.stockService.getStockProductbyCodProductoExite(this.codigoProducto, this.idPuntosVenta).subscribe(result => {

      if (Object.keys(result).length === 0) {
        this.cantidad = 0;
        this.precioDis = 0;
        this.precioMay = 0;

        this.precioUnit = 0;
        this.precioBulto = 0;
        this.detalle = "";
        this.cantidadDisponible = 0;
        this.precioSeleccionado = 0;



      } else {

        this.cantidadDisponible = 0;
        this.cantidadConsulta = 0;
        this.cantidadLista = 0;
        this.cantidadConsulta = result[0].cantidad;
        (<HTMLInputElement>document.getElementById("primerRadio")).checked = true;
        if (this.listaDetalleFactura.length == 0) {
          this.cantidadDisponible = this.cantidadConsulta;
        } else {

          for (let i = 0; i < this.listaDetalleFactura.length; i++) {
            if (this.listaDetalleFactura[i].catStock.id.idProductos == result[0].catProducto.idProductos
              && this.listaDetalleFactura[i].catStock.id.idPuntosVenta == this.idPuntosVenta

            ) {

              this.cantidadLista = Number(this.listaDetalleFactura[i].cantidadFact);
              this.cantidadDisponible = this.cantidadConsulta - this.cantidadLista;


              break;

            } else {
              this.cantidadDisponible = this.cantidadConsulta;
            }
          }
        }

        this.idProductoConsulta = Number(result[0].catProducto.idProductos);

        this.cantidad = 1;
        //this.cantidadDisponible = result[0].cantidad;
        this.precioDis = result[0].precioDistribuidor;
        this.precioMay = result[0].precioMayor;
        this.precioUnit = result[0].precioUnit;
        this.precioBulto = result[0].precioBulto;

        this.precioSeleccionado = this.precioUnit;
        this.detalle = result[0].catProducto.catCategoria.nombreCategoria + " " +
          result[0].catProducto.detalle + " - " +
          result[0].catProducto.catMedida.abreviacion + " ";

      }

    }, err => console.log(err))




  }




  ///Metodo de eliminar un producto de la lista que se va a vender
  eliminarProductodeList(idProducto: number) {

    //console.log("este es el id del producto escocigido", idProducto);

    this.listaDetalleFactura = this.listaDetalleFactura.filter(producto => {
      return producto.catStock.id.idProductos != idProducto;
    })
    let totalvista = 0;
    this.subtotalFactura = 0;
    this.ivaFactura = 0;
    this.subtotalIva0 = 0;
    this.subtotalIva12 = 0;
    for (var x in this.listaDetalleFactura) {

      if (this.listaDetalleFactura[x].ivaProducto === 0) {
        this.subtotalIva0 = this.subtotalIva0 + this.listaDetalleFactura[x].valorTotalSinIva;
      }
      if (this.listaDetalleFactura[x].ivaProducto !== 0) {
        this.subtotalIva12 = this.subtotalIva12 + this.listaDetalleFactura[x].valorTotalSinIva;
      }

      totalvista += (this.listaDetalleFactura[x].valorTotal);

      this.subtotalFactura += this.listaDetalleFactura[x].valorTotalSinIva;
    }


    this.subtotalFactura = Math.round(this.subtotalFactura * 100) / 100;

    this.subtotalIva12 = Math.round(this.subtotalIva12 * 100) / 100;
    this.subtotalIva0 = Math.round(this.subtotalIva0 * 100) / 100;

    this.ivaFactura = Math.round((totalvista - this.subtotalFactura) * 100) / 100;

    this.totalIngresoVista = "" + totalvista;
    this.totalVenta = "" + Math.round((totalvista) * 100) / 100;
    this.totalVentaAxuliar = "" + totalvista;



  }
  //////METODO PARA REALIZAR LA BUSQUEDA DE UN CLIENTE POR LA CEDULA
  async buscarClienteByCedula() {
    this.clienteService.getClienteByCedula(this.cedula).subscribe(result => {

      if (Object.keys(result).length === 0) {
        this.nombreCliente = "";
        this.apellidoCliente = "";
        this.telefono = "";
        this.email = "";
        this.direccion = "";
        this.codigoCedula = "";
      } else {

        // console.log(result)
        this.nombreCliente = result[0].nombreCli;
        this.apellidoCliente = result[0].apellidoCli;
        this.telefono = result[0].telefono;
        this.email = result[0].email;
        this.direccion = result[0].direccionCli;
        this.codigoCedula = result[0].codigo;
      }
    }, err => console.log(err))

  }

  showConsultar() {
    this.displayConsultar = true;
  }
  showCliente() {
    this.displayCliente = true;
  }

  getStockConsulta(id: number) {
    //console.log(this.idPuntosVenta)
    this.stockService.findStockInventarioPuntoVenta(id).subscribe(res => { this.stockConsulta = res }, err => console.log(err))
  }

  encontrarProductoModal(productoBuscar: any) {
    // console.log(productoBuscar.length)
    if (productoBuscar.length != 0) {
      this.stockService.findStockbyParametersPuntoVenta(this.idPuntosVenta, productoBuscar).subscribe(res => {
        this.stockConsulta = res;
      }, err => console.log(err));
    }
    else {
      this.getStockConsulta(this.idPuntosVenta);
    }

  }

  ingresarClienteModal() {
    //console.log(this.clientmodal)
  }

  favoriteSeason: string;
  seasons: string[] = ['Winter', 'Spring', 'Summer', 'Autumn'];

  //calcular descuento al total de la factura
  calcularDescuento($event: any) {
    this.porcentajeDescuentoSeleccionado = $event.target.value;


    this.subtotalFactura = Number(this.totalVentaAxuliar);

    let valorDescuento = Number(this.totalVentaAxuliar) * (Number(this.porcentajeDescuentoSeleccionado / 100));

    this.descuentoFactura = Number(valorDescuento.toFixed(2));
    this.totalVenta = String(Number(this.subtotalFactura) - Number(valorDescuento));

    //console.log(this.totalVenta);
  }

  async llenarDatos() {
    this.totalImpuestoAux = [];

    this.disabled = false;
    if (this.tipoPago === "") {

      this.notificacion.showError('Seleccione tipo de pago', '**Error')
    } else {


      let idfacturaPDF = 0;
      let fecha = new Date()
      let fechaFormateada = fecha.getFullYear() + "-" + ("0" + (fecha.getMonth() + 1)).slice(-2) + "-" + ("0" + (fecha.getDate() + 1)).slice(-2);
      //console.log(fechaFormateada);
      this.auxiliarFacturaIngreso.fechaFactu = fechaFormateada;
      this.auxiliarFacturaIngreso.estado = "A";
      this.auxiliarFacturaIngreso.tipoPago = this.tipoPago;
      this.auxiliarFacturaIngreso.iva = this.porcentajeDescuentoSeleccionado;
      this.auxiliarFacturaIngreso.total = (Number(this.totalVenta));
      this.auxiliarFacturaIngreso.subtotal = (Number(this.subtotalFactura));
      this.auxiliarFacturaIngreso.descuento = (Number(this.descuentoFactura));
      this.auxiliarFacturaIngreso.iva = (Number(this.ivaFactura));
      this.auxiliarFacturaIngreso.usUser.idUsuario = this.usuarioId;//Usuario logeaado 
      this.auxiliarFacturaIngreso.venCliente.idCliente = this.idClienteIngreso;
      if (this.listaDetalleFactura.length === 0) {
        this.notificacion.showError('No hay datos para realizar la venta', '**Error')
        // this.isloading=true;
      } else {

        this.isloading = true;


        for (let i = 0; i < this.listaDetalleFactura.length; i++) {
          this.auxiliarFacturaIngreso.detallefact[i] = {

            cantidadFact: Number(this.listaDetalleFactura[i].cantidadFact),
            descripcion: this.listaDetalleFactura[i].descripcion,
            valorTotal: Number(this.listaDetalleFactura[i].valorTotal),
            valorTotalSinIva: Number(this.listaDetalleFactura[i].valorTotalSinIva),
            valorUnit: Number(this.listaDetalleFactura[i].valorUnit),
            valorUnitSinIva: Number(this.listaDetalleFactura[i].valorUnitSinIva),
            ivaProducto: Number(this.listaDetalleFactura[i].ivaProducto),
            catStock: {
              id: {
                idPuntosVenta: Number(this.listaDetalleFactura[i].catStock.id.idPuntosVenta),
                idProductos: Number(this.listaDetalleFactura[i].catStock.id.idProductos)
              }
            }




          }
        }


        // console.log(this.auxiliarFacturaIngreso);



      }
    }
    this.isloading = false;
  }
  ultimaFactura: any = [];
  secuencial;
  numeroFacturero;
  numeroPtoEmision;
  numeroGenerado;

  async generarNumeroSecuencialNumeroFactura() {

    const arrayFacturas = new Promise(async (resolve, reject) => {
      await this.facturaService.consultaSecuencial().subscribe(res => {

        resolve(res);

      }, err => console.log(err))
    });

    await arrayFacturas.then(res => {
      if (Object.entries(res).length == 0) {

      } else {

        this.ultimaFactura = res[0];
        // console.log(this.ultimaFactura)
      }

    })

    if (Object.entries(this.ultimaFactura).length === 0) {
      // no hay facturas
      this.secuencial = "000000001";
      this.numeroFacturero = "001";
      this.numeroPtoEmision = "001";
      // if (this.ultimaFactura.estado === "A") {
      //   this.ultimaFactura.estado = "APROBADO";

      // }
    } else {

      // console.log(this.ultimaFactura)
      this.secuencial = Number(this.ultimaFactura.secuencial);
      this.numeroFacturero = Number(this.ultimaFactura.ptoEmision)
      this.numeroPtoEmision = Number(this.ultimaFactura.estab)

      if (Number(this.secuencial) < 999999999) {
        this.secuencial = this.secuencial + 1;

        this.generarNumeroConCerosIzquierda(9, this.secuencial);
        this.secuencial = this.numeroGenerado;

        this.numeroFacturero = this.numeroFacturero;
        this.generarNumeroConCerosIzquierda(3, this.numeroFacturero);
        this.numeroFacturero = this.numeroGenerado;

      } else {
        this.secuencial = 1;
        this.generarNumeroConCerosIzquierda(9, this.secuencial);
        this.secuencial = this.numeroGenerado;

        //generamos numero del facturero
        this.numeroFacturero = this.numeroFacturero + 1;
        this.generarNumeroConCerosIzquierda(3, this.numeroFacturero);
        this.numeroFacturero = this.numeroGenerado;

      }
    }


  }

  async generarNumeroConCerosIzquierda(tamaño: number, numero) {
    this.numeroGenerado = numero;

    var numberOutput = Math.abs(this.numeroGenerado); /* Valor absoluto del número */
    var length = this.numeroGenerado.toString().length; /* Largo del número */
    var zero = "0"; /* String de cero */

    if (tamaño <= length) {
      if (this.numeroGenerado < 0) {
        this.numeroGenerado = ("-" + numberOutput.toString());
      } else {
        this.numeroGenerado = (numberOutput.toString());
      }
    } else {
      if (this.numeroGenerado < 0) {
        this.numeroGenerado = (("-" + (zero.repeat(tamaño - length)) + numberOutput.toString()));
      } else {
        this.numeroGenerado = (((zero.repeat(tamaño - length)) + numberOutput.toString()));
      }
    }


  }


}
