import { Component, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

//Stock
import { cat_stock } from '../../models/cat_stock';
import { CatStockService } from '../../services/cat-stock.service';
//Puntos de venta
import { PuntosVentasService } from '../../services/puntos-ventas.service';
import { PuntosVentas } from '../../models/cat_PuntosVenta';
//notificacion
import { NotificacionService } from '../../services/notificacion.service';

import { ProductoService } from 'src/app/services/producto.service';
@Component({
  selector: 'app-discount',
  templateUrl: './discount.component.html',
  styleUrls: ['./discount.component.css']
})
export class DiscountComponent implements OnInit {

  constructor(private stockService: CatStockService, 
    private productServices: ProductoService,

    private puntoventaservice: PuntosVentasService, 
    private activedrouter: ActivatedRoute, private router: Router,
    private notificacion: NotificacionService) { }
    puntoVenta: PuntosVentas ;
    puntoVentaEscogido: any = [];
    stock: any = [];
    displayProductos: boolean = false;
    displayDescuento:boolean=false;
    idPuntoVentaPrueba: number = 0;
    idProductoObtenido:number =0;
    idProductoStock:number =0;

    precioDis:number = 0;
    precioBulto:number = 0;
    precioMay:number = 0;
    precioUnit:number = 0;
    cantidad:number = 0;
    stockMax:number = 0;
    stockMin:number = 0;

    titulo:any;
    codigoTitulo:any;
    detalleTitulo:any;
 
  ngOnInit() {
    this.getPuntosVenta();
    this.displayProductos = false;
    this.displayDescuento = false;
  }
  isEnable(x:any){
    
    
    if(x==null){
      this.stock = [];
    }
  }
  getPuntosVenta() {
    this.puntoventaservice.getPuntosVentas()
      .subscribe(res => {
        this.puntoVenta = res;
        // console.log(this.puntoVenta)
      });
  }
  
  consulta(){
    
    try {
      if(typeof(this.puntoVentaEscogido.idPuntosVenta)!='number'){
        this.notificacion.showError('No ha seleccionado ningun Punto de venta','**Punto de venta')
        this.displayProductos = false;
        this.idPuntoVentaPrueba = 0;
        this.stock = [];
      }else{
        this.displayProductos = true;
        this.idPuntoVentaPrueba = this.puntoVentaEscogido.idPuntosVenta;
       this.encontrarproducto(this.puntoVentaEscogido.idPuntosVenta);
  
      }
    } catch (error) {
      this.displayProductos = false;
      this.idPuntoVentaPrueba = 0;
      this.stock = [];
    this.notificacion.showError('No ha seleccionado ningun Punto de venta','**Punto de venta')
      
    }
    

  }

  encontrarProductoModal(buscarProducto:any){
    if(buscarProducto.length!=0){
      this.stockService.findStockbyParametersPuntoVenta(this.idPuntoVentaPrueba,buscarProducto).subscribe(res=>{
        this.stock = res;
      },err=>console.log(err));
    }else{
      this.encontrarproducto(this.idPuntoVentaPrueba);
    }
  }

  async encontrarproducto(idpuntoventa:number){
    await this.stockService.findStockInventarioPuntoVenta(Number(idpuntoventa)).subscribe(res=>{
          
      this.stock = res;
    },err=>console.log("error",err))
  }

  obtenerVariable(id:number){
    this.idProductoObtenido = id;
    this.displayDescuento = true;
    this.encontrarStockProducto();
  }

  async encontrarStockProducto(){

    const IDPRODUCTO = new Promise(async (resolve, reject) => {
      await this.productServices.findproductobycodigo(String(this.idProductoObtenido)).subscribe((res) => {
        resolve(res)
        //console.log("idProductos consulta", res)

      }, err => console.log(err))
    });


    await IDPRODUCTO.then(res => {
      
      this.idProductoStock = Number(res);
      if (Number(res) > 0) {
        
          //bsucar producto en stock de acuerdo al id de puntos de venta y al id del producto 
        this.stockService.findbyIdproductoIdpuntosVenta(Number(res), this.idPuntoVentaPrueba).subscribe(result => {
          //////////////
          // console.log("envia =>", result, "que tipo es", typeof (result));
          if (Object.keys(result).length === 0) {

            
          
            this.cantidad = 0;
            this.precioDis = 0;
            this.precioBulto = 0;
            this.precioMay = 0;
            this.precioUnit = 0;
            this.stockMax = 0;
            this.stockMin = 0;
         

          } else {
            this.titulo=result[0].catProducto.catCategoria.nombreCategoria;
    this.codigoTitulo=result[0].catProducto.codProducto;
    this.detalleTitulo=result[0].catProducto.detalle;

            this.cantidad = result[0].cantidad
            this.precioDis = result[0].precioDistribuidor;
            this.precioBulto = result[0].precioBulto;
            this.precioMay = result[0].precioMayor;
            this.precioUnit = result[0].precioUnit;
            this.stockMax = result[0].stockMax;
            this.stockMin = result[0].stockMin;

            // console.log(" ", result[0].catProducto.catCategoria.nombreCategoria, " ",result[0].catProducto.detalle )
            // console.log(this.cantidad," ",this.precioDis,
            // this.precioBulto," ",
            // this.precioMay," ", 
            // this.precioUnit," ",
            // this.stockMax," ", 
            // this.stockMin )
          }


          //console.log(result)


        }, err => console.log(err))
      } else {
        this.titulo="";
        this.codigoTitulo=0;
        this.detalleTitulo="";
        this.cantidad = 0;
        this.precioDis = 0;
        this.precioBulto = 0;
        this.precioMay = 0;
        this.precioUnit = 0;
        this.stockMax = 0;
        this.stockMin = 0;
        this.idProductoStock=0;
        
      }//aqui


    })
  }

  async actualizarDescuento(){
    
    
    const ActualizarPrecio = new Promise(async (resolve,reject)=>{
await this.stockService.updateStocks(
  Number(this.cantidad),
  Number(this.precioUnit),
  Number(this.precioMay),
  Number(this.precioDis),
  Number(this.precioBulto),
  Number(this.stockMax),
  Number(this.idProductoStock),
  Number(this.stockMin),
  'S',
  Number(this.idPuntoVentaPrueba)
).subscribe(res => {
  resolve(res)
  this.titulo="";
this.codigoTitulo=0;
this.detalleTitulo="";
    this.cantidad = 0;
    this.precioDis = 0;
    this.precioBulto = 0;
    this.precioMay = 0;
    this.precioUnit = 0;
    this.stockMax = 0;
    this.stockMin = 0;
  this.displayDescuento = false;
}, err => console.log(err))

    }) 

    await ActualizarPrecio.then(res=>res);
   await this.encontrarproducto(this.idPuntoVentaPrueba);
   await this.notificacion.showSuccess('Se actualizo el Precio *** No te olvides de volver a dejarle como antes','****Descuento')
  }
  

}
