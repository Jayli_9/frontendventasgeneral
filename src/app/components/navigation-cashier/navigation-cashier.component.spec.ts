import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NavigationCashierComponent } from './navigation-cashier.component';

describe('NavigationCashierComponent', () => {
  let component: NavigationCashierComponent;
  let fixture: ComponentFixture<NavigationCashierComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigationCashierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationCashierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
