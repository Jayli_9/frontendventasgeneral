import { Component, HostBinding, OnInit } from '@angular/core';
import { MedidaService }from '../../../services/medida.service';
import { Medidas } from '../../../models/cat_Medida';
import { NotificacionService } from '../../../services/notificacion.service';

@Component({
  selector: 'app-medida-list',
  templateUrl: './medida-list.component.html',
  styleUrls: ['./medida-list.component.css']
})
export class MedidaListComponent implements OnInit {
  @HostBinding('class') classes = 'row';
  medidas : any = [] ;
  selectedmedidas: Medidas;
  constructor( private medidaservice : MedidaService,
    private notificacion: NotificacionService) { }

  ngOnInit() {
    this.getMedidas();
  }

  getMedidas(){
    this.medidaservice.getMedidas().subscribe(
      res => {
        this.medidas = res
      },
      err => console.error(err)
    );
  }

  deleteMedida(id: number)
  {
    this.medidaservice.deleteMedida(id).subscribe(
      res => {
        setTimeout(()=>{
          this.notificacion.showInfo('La medida se ha eliminado','Medida eliminada');

        },200);
        this.getMedidas();
      },
      err => console.error(err)
    );

  }

}
