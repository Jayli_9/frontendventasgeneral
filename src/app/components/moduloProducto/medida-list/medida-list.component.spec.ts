import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MedidaListComponent } from './medida-list.component';

describe('MedidaListComponent', () => {
  let component: MedidaListComponent;
  let fixture: ComponentFixture<MedidaListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MedidaListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedidaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
