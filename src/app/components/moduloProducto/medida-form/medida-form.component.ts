import { Component, HostBinding, OnInit } from '@angular/core';
import { Medidas } from '../../../models/cat_Medida';
import { MedidaService } from '../../../services/medida.service';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificacionService } from '../../../services/notificacion.service';
declare let $ : any ;

@Component({
  selector: 'app-medida-form',
  templateUrl: './medida-form.component.html',
  styleUrls: ['./medida-form.component.css']
})
export class MedidaFormComponent implements OnInit {
  @HostBinding('class') classes = 'row';
  medidas : Medidas = {
    abreviacion:'',
    medida:'',
    tipo:''
  }
  edit : boolean = false;
  creacion:string = '';
  constructor(private medidasService: MedidaService, 
              private router: Router,
              private activedrouter: ActivatedRoute,
              private notificacion: NotificacionService) { }

  ngOnInit() {
    this.creacion = 'Crear';
    const params = this.activedrouter.snapshot.params;
    if(params.id){
      this.creacion = 'Actualizar';
      this.medidasService.getMedida(params.id).subscribe(
        res=>{
          if(res!= null){
            this.medidas = res; 
            this.edit = true;

          }else{
            this.router.navigate(['/size']);
          }
          
        },
        err => console.log("hay error "+ err)
      )
    }
  }

  saveMedidas(){
    let medida = this.quitarespacios('#medida');
    let abreviacion = this.quitarespacios('#abreviacion');
    let tipo=this.quitarespacios('#tipo')
    if(medida.length>0){
      this.medidas.abreviacion = abreviacion;
      this.medidas.medida = medida;
      this.medidas.tipo =tipo;

      this.medidasService.saveMedida(this.medidas).subscribe(
        res=>{
          this.medidas.abreviacion ='';
          this.medidas.medida='';
          this.medidas.tipo ='';
          setTimeout(()=>{
            this.notificacion.showSuccess('La medida se ha agregado correctamente','Medida agregada');
          },200)
          this.router.navigate(['/admin/size']);
        },error => console.error(error)
      );
    }else{
      this.notificacion.showError('Revise si los campo estan llenos','**Error al Agergar Medida')
    }
  }

  updateMedidas(){
    let abreviacion = this.quitarespacios('#abreviacion');
    let medida = this.quitarespacios('#medida');
    let tipo=this.quitarespacios('#tipo')
    if(medida.length > 0){
      this.medidas.abreviacion = abreviacion;
      this.medidas.medida = medida;
      this.medidas.tipo = tipo;
      this.medidasService.updateMedida(this.medidas.idMedidas ,this.medidas).subscribe(
        res => {
          this.medidas.abreviacion = '';
          this.medidas.medida = '';
          this.medidas.tipo = '';
          setTimeout(()=>{
            this.notificacion.showSuccess('La medida se ha actualizado correctamente','Medida actualizado');
          },200)

          this.router.navigate(['/admin/size'])
        },
        err => console.error(err)
      );
    }else{
      this.notificacion.showError('Revise si estan llenos los campos','**Error al actualizar Medida')
    }
  }

  quitarespacios(atributoHTML: string){
    let obtenerletras = $(atributoHTML).val();

    return obtenerletras.trim();
  }

}
