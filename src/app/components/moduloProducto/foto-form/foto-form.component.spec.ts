import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DisenosFormComponent } from './foto-form.component';

describe('DisenosFormComponent', () => {
  let component: DisenosFormComponent;
  let fixture: ComponentFixture<DisenosFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DisenosFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisenosFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
