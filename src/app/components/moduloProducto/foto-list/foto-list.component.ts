import { Component, HostBinding, OnInit } from '@angular/core';
import { FotosService } from '../../../services/fotos.service';
import { Foto } from '../../../models/cat_foto';
import { FormGroup } from '@angular/forms';
import { NotificacionService } from '../../../services/notificacion.service';

@Component({
  selector: 'app-foto-list',
  templateUrl: './foto-list.component.html',
  styleUrls: ['./foto-list.component.css']
})
export class DisenosListComponent implements OnInit {
  @HostBinding('class') classes = 'row';
  disenos: any = [];
  selectedDisenos : Foto;
  
  constructor(private fotosServices : FotosService,
    private notificacion: NotificacionService) { }

  ngOnInit() {
    this.getDisenos();
  }

  getDisenos(){
    // this.fotosServices.getDisenos().subscribe(
    //   res => {
    //     this.disenos = res
    //   },
    //   err => console.error(err)
    // );
  }

  deleteDisenos(id: number)
  {
    // this.fotosServices.deleteDiseno(id).subscribe(
    //   res => {
    //     setTimeout(()=>{
    //       this.notificacion.showInfo('El diseno se ha eliminado','Diseno eliminado');

    //     },200);
    //     this.getDisenos()
    //   },
    //   err => console.error(err)
    // );

  }


}
