import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DisenosListComponent } from './foto-list.component';

describe('DisenosListComponent', () => {
  let component: DisenosListComponent;
  let fixture: ComponentFixture<DisenosListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DisenosListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisenosListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
