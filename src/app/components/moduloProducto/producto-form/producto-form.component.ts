import { Component, HostBinding, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
//Productos
import { Productos } from '../../../models/cat_Producto';
import { ProductoService } from '../../../services/producto.service';
//Tallas
import { Medidas } from '../../../models/cat_Medida';
import { MedidaService } from '../../../services/medida.service';
//Disenos
import { Foto } from '../../../models/cat_foto';
import { FotosService } from '../../../services/fotos.service';
// categoria 
import { Categorias } from '../../../models/cat_Categoria';
import { CategoriaService } from '../../../services/categoria.service';
// Porcentaje 
import { Porcentaje } from '../../../models/cat_porcentaje';
import { PorcentajesService } from '../../../services/porcentajes.service';

import { NotificacionService } from '../../../services/notificacion.service';
import { DomSanitizer } from '@angular/platform-browser';




declare let $: any;

@Component({
  selector: 'app-producto-form',
  templateUrl: './producto-form.component.html',
  styleUrls: ['./producto-form.component.css']
})
export class ProductoFormComponent implements OnInit {
  @HostBinding('class') classes = 'row';
  //categorias
  categoria: Categorias;
  categoriaEscogida: any = [];
  // uploadedFiles: any[] = [];
  //medida

  medidas: Medidas;
  medidaEscogida: any = [];
  //porcentaje
  porcentaje: Porcentaje;
  porcentajeEscogida: any = [];


  imagenObtenidaMostrar1: any;
  imagenObtenidaMostrar2: any;
  imagenObtenidaMostrar3: any;

  imagenObtenidaAnteriorUrl1: any;
  imagenObtenidaAnteriorUrl2: any;
  imagenObtenidaAnteriorUrl3: any;
  isloading = false;
  imagenObtenidaIngresar1: any;
  imagenObtenidaIngresar2: any;
  imagenObtenidaIngresar3: any;
  idProductoencontrado: number = 0;
  existe: boolean = false;





  productos: Productos = {

    catCategoria: { idCategoria: 0 },
    catMedida: { idMedidas: 0 },
    catPorcentaje: { idPorcentajes: 0 },
    detalle: '',
    codProducto: '',
    urlFoto1: '',
    urlFoto2: '',
    urlFoto3: '',
    valorMedida: '',

  }
  productosEscogidos: any[];
  edit: boolean = false;
  creacion: string = '';
  codigoProductoAnterior: string = '';



  constructor(private productoservices: ProductoService,
    private categoriaservices: CategoriaService,
    private fotosService: FotosService,
    private medidaservice: MedidaService,
    private porcentajeService: PorcentajesService,
    private activedrouter: ActivatedRoute, private router: Router,
    private notificacion: NotificacionService, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.creacion = 'Crear';
    const params = this.activedrouter.snapshot.params;

    if (params.id) {
      this.creacion = 'Actualizar';
      this.productoservices.getProducto(params.id).subscribe(
        res => {
          if (res != null) {
            //console.log(res);
            this.productos = res;
            this.medidaservice.getMedida(this.productos.catMedida.idMedidas).subscribe(
              res => {
                this.medidaEscogida = res;

              }, error => console.error(error)
            );
            this.categoriaservices.getCategoria(this.productos.catCategoria.idCategoria).subscribe(
              res => {
                this.categoriaEscogida = res;

              },
              error => console.error(error)
            );
            this.porcentajeService.getPorcentaje(this.productos.catPorcentaje.idPorcentajes).subscribe(
              res => {
                this.porcentajeEscogida = res;

              },
              error => console.error(error)
            );
            ///seleccionar imagenes para mostrar
            this.imagenObtenidaMostrar1 = this.productos.urlFoto1;
            this.imagenObtenidaAnteriorUrl1 = this.productos.urlFoto1;
            this.imagenObtenidaMostrar2 = this.productos.urlFoto2;
            this.imagenObtenidaAnteriorUrl2 = this.productos.urlFoto2;
            this.imagenObtenidaMostrar3 = this.productos.urlFoto3;
            this.imagenObtenidaAnteriorUrl3 = this.productos.urlFoto3;
            this.codigoProductoAnterior = this.productos.codProducto;




            this.edit = true;

          } else {
            this.router.navigate(['/admin/product']);
          }

        },
        err => console.log("hay error " + err)
      )
    }
    this.getMedidas();

    this.getCategorias();

    this.getPorcentajes();


  }

  async saveProductos() {

    await this.buscarStockProducto();

    if (this.existe) {
      this.notificacion.showError('El código ingresado corresponde a otro producto, por favor ingrese un nuevo código', '**Error al agregar producto')

    } else {


      this.isloading = true;
      let x = Math.floor(Math.random() * (1000 - 1)) + 1;
      // if (this.productos.urlfoto1.length > 0) {

      if (!this.imagenObtenidaIngresar1) {
        //no hace nd por que no encontro un archivo para subir 
      } else {

        console.log(this.imagenObtenidaIngresar1);
        const urlNueva1 = new Promise(async (resolve, reject) => {
          await this.fotosService.uploadImage(this.imagenObtenidaIngresar1, x.toString()).then(res => {
            resolve(res);
            // console.log("hola pe")
          }, err => console.log("hola pe"))
        });

        await urlNueva1.then(res => this.productos.urlFoto1 = String(res));
      }

      if (!this.imagenObtenidaIngresar2) {

      } else {

        const urlNuevaFoto2 = new Promise(async (resolve, reject) => {
          await this.fotosService.uploadImage(this.imagenObtenidaIngresar2, x.toString()).then(res => {
            resolve(res);
            // console.log("hola pe")

          }, err => console.log("hola pe"))
        });

        await urlNuevaFoto2.then(res => this.productos.urlFoto2 = String(res));
      }
      if (!this.imagenObtenidaIngresar3) {
      } else {

        const urlNuevaFoto3 = new Promise(async (resolve, reject) => {
          await this.fotosService.uploadImage(this.imagenObtenidaIngresar3, x.toString()).then(res => {
            resolve(res);
            console.log("hola pe")
          }, err => console.log("hola pe"))
        });

        await urlNuevaFoto3.then(res => this.productos.urlFoto3 = String(res));


      }


      if (this.testingreso()) {

        this.productoservices.saveProducto(this.productos).subscribe(
          res => {
            setTimeout(() => {
              this.isloading = false;
              this.notificacion.showSuccess('El Producto se agrego correctamente', 'Producto agregado');
            }, 200);
            this.router.navigate(['/admin/product'])
            this.isloading = false;

          }, error => console.error(error)
        );

      } else {
        this.notificacion.showError('Revise si todos los campo esten llenos', '**Error al agregar producto')
      }
      this.isloading = false;
    }
  }

  async updateProductos() {

    await this.buscarStockProductoActualizar();

    if (this.existe) {
      this.notificacion.showError('El código ingresado corresponde a otro producto, por favor ingrese un nuevo código', '**Error al actualizar producto')


    } else {

      this.isloading = true;
      //cuando en el ingreso la imagen no tiene crgado no me muetsra nada, 
      //por el contrario me toca subir una imagen
      this.isloading = true;
      let x = Math.floor(Math.random() * (1000 - 1)) + 1;

      //si la imagen contiene un archivo 

      // if (typeof this.imagenObtenidaIngresar1 === 'undefined') {
      //   console.log("esta indefinida");
      // }

      if (!this.imagenObtenidaIngresar1) {

      } else {


        //validación si la imagen esta vacia tenemos que ingresar una nueva al firebase

        const urlNueva1 = new Promise(async (resolve, reject) => {
          await this.fotosService.uploadImage(this.imagenObtenidaIngresar1, x.toString()).then(res => {
            resolve(res);

          }, err => console.log("hola pe"))

        });

        await urlNueva1.then(res => this.productos.urlFoto1 = String(res));


      }
      //si la imagen 2 esta vacia tenemos que ingresar una nueva imagen al firebase
      if (!this.imagenObtenidaIngresar2) {


      } else {

        const urlNueva2 = new Promise(async (resolve, reject) => {
          await this.fotosService.uploadImage(this.imagenObtenidaIngresar2, x.toString()).then(res => {
            resolve(res);

          }, err => console.log("hola pe"))
        });

        await urlNueva2.then(res => this.productos.urlFoto2 = String(res));


      }
      //si la imagen 3 esta vacia tenemos que ingresar una nueva imagen al firebase
      if (!this.imagenObtenidaIngresar3) {


      } else {

        const urlNueva3 = new Promise(async (resolve, reject) => {
          await this.fotosService.uploadImage(this.imagenObtenidaIngresar3, x.toString()).then(res => {
            resolve(res);

          }, err => console.log("hola pe"))
        });

        await urlNueva3.then(res => this.productos.urlFoto3 = String(res));


      }



      /////////validacion si tiene una url anterior eliminar pirmero antes de ingresar nueva imagen 
      if (!this.imagenObtenidaAnteriorUrl1) {



        if (this.imagenObtenidaAnteriorUrl1 === "") {

        } else {

          this.fotosService.borrarImagen(this.imagenObtenidaAnteriorUrl1).then(res => res);
        }

      } else {

      }

      if (!this.imagenObtenidaAnteriorUrl2) {
        if (this.imagenObtenidaAnteriorUrl2 === "") {

        } else {


          this.fotosService.borrarImagen(this.imagenObtenidaAnteriorUrl2).then(res => res);
        }
      } else {
      }

      if (!this.imagenObtenidaAnteriorUrl3) {
        if (this.imagenObtenidaAnteriorUrl3 === "") {

        } else {


          this.fotosService.borrarImagen(this.imagenObtenidaAnteriorUrl3).then(res => res);
        }
      } else {

      }

      /////metodo para validar y actualizar el producto 
      if (this.testingreso()) {
        
        this.productoservices.updateProducto(this.productos.idProductos, this.productos).subscribe(
          res => {
            setTimeout(() => {
              this.notificacion.showSuccess('El producto se ha actualizado correctamente', 'Producto actualizado');

            }, 200);
            this.router.navigate(['/admin/product'])
          }, error => { console.error(error) }
        );
      } else {
        this.notificacion.showError('Revisar si selecciono un Usuario o un Rol', '** Error al Actualizar los Roles de Usuarios')
      }
      this.isloading = false;
    }
  }



  getMedidas() {
    this.medidaservice.getMedidas().subscribe(
      res => {
        this.medidas = res;
      }, error => console.error(error)
    );
  }

  getCategorias() {
    this.categoriaservices.getCategorias().subscribe(
      res => {
        this.categoria = res;

      }, error => console.error(error)
    );
  }
  getPorcentajes() {
    this.porcentajeService.getPorcentajes().subscribe(
      res => {
        this.porcentaje = res;

      }, error => console.error(error)
    );
  }


  async testingreso() {

    if (this.medidaEscogida.length != 0 &&
      this.categoriaEscogida.length != 0) {



      this.productos.catMedida.idMedidas = this.medidaEscogida.idMedidas;
      this.productos.catCategoria.idCategoria = this.categoriaEscogida.idCategoria;
      this.productos.catPorcentaje.idPorcentajes = this.porcentajeEscogida.idPorcentajes;

      

      return true;
    } else {

      return false;
    }
  }



  //////////////////////////capturar imagenes
  onBasicUpload1(file: any) {

    //  this.productoservices.uploadImage(file.target.files[0],x.toString())
    // .then(res=>{this.imagenObtenida=res})
    this.blobFile1(file.target.files[0]).then((res: any) => {
      this.imagenObtenidaMostrar1 = res.base;

    })

    this.imagenObtenidaIngresar1 = file.target.files[0];
    //console.log(this.imagenObtenidaIngresar)
  }
  blobFile1 = async ($event: any) => new Promise((resolve, reject) => {
    try {
      const unsafeImg = window.URL.createObjectURL($event);
      const image = this.sanitizer.bypassSecurityTrustUrl(unsafeImg);
      const reader = new FileReader();
      reader.readAsDataURL($event);
      reader.onload = () => {
        resolve({
          blob: $event,
          image,
          base: reader.result
        });
      };
      reader.onerror = error => {
        resolve({
          blob: $event,
          image,
          base: null
        });
      };

    } catch (e) {
      return null;
    }
  }
  )


  /////////////////////////segunda imagen
  onBasicUpload2(file: any) {

    //  this.productoservices.uploadImage(file.target.files[0],x.toString())
    // .then(res=>{this.imagenObtenida=res})
    this.blobFile2(file.target.files[0]).then((res: any) => {
      this.imagenObtenidaMostrar2 = res.base;
    })

    this.imagenObtenidaIngresar2 = file.target.files[0];
    //console.log(this.imagenObtenidaIngresar)
  }

  blobFile2 = async ($event: any) => new Promise((resolve, reject) => {
    try {
      const unsafeImg = window.URL.createObjectURL($event);
      const image = this.sanitizer.bypassSecurityTrustUrl(unsafeImg);
      const reader = new FileReader();
      reader.readAsDataURL($event);
      reader.onload = () => {
        resolve({
          blob: $event,
          image,
          base: reader.result
        });
      };
      reader.onerror = error => {
        resolve({
          blob: $event,
          image,
          base: null
        });
      };

    } catch (e) {
      return null;
    }
  })
  /////////////////////tercera imagen
  onBasicUpload3(file: any) {

    //  this.productoservices.uploadImage(file.target.files[0],x.toString())
    // .then(res=>{this.imagenObtenida=res})
    this.blobFile3(file.target.files[0]).then((res: any) => {
      this.imagenObtenidaMostrar3 = res.base;
    })

    this.imagenObtenidaIngresar3 = file.target.files[0];
    //console.log(this.imagenObtenidaIngresar)
  }

  blobFile3 = async ($event: any) => new Promise((resolve, reject) => {
    try {
      const unsafeImg = window.URL.createObjectURL($event);
      const image = this.sanitizer.bypassSecurityTrustUrl(unsafeImg);
      const reader = new FileReader();
      reader.readAsDataURL($event);
      reader.onload = () => {
        resolve({
          blob: $event,
          image,
          base: reader.result
        });
      };
      reader.onerror = error => {
        resolve({
          blob: $event,
          image,
          base: null
        });
      };

    } catch (e) {
      return null;
    }
  })

  ///////////////////////////////////



  async buscarStockProducto() {

    const IDPRODUCTO = new Promise(async (resolve, reject) => {
      await this.productoservices.findproductobycodigo(this.productos.codProducto).subscribe((res) => {
        resolve(res)
        //console.log("idProductos consulta", res)

      }, err => console.log(err))
    });


    await IDPRODUCTO.then(res => {
      this.idProductoencontrado = Number(res);

      //cosuotar producto por id para llenar lista de los select 
      ////////////////
      if (this.idProductoencontrado > 0) {
        //cambio el valor de la variale para desactivar el boton aceptar si encuentra el producto
        this.existe = true;

      } else {

        this.existe = false;

      }
    })

  }
  async buscarStockProductoActualizar() {

    const IDPRODUCTO = new Promise(async (resolve, reject) => {
      await this.productoservices.findproductobycodigo(this.productos.codProducto).subscribe((res) => {
        resolve(res)
        //console.log("idProductos consulta", res)

      }, err => console.log(err))
    });
    let idProductoActualizar = Number(this.productos.codProducto);
    let ProductoEncontrado: any;

    await IDPRODUCTO.then(res => {
      this.idProductoencontrado = Number(res);

      //cosuotar producto por id para llenar lista de los select 
      ////////////////
    })


    //realizar consulta para consultar el codigo del producto de acuerdo al id 
    const PRODUCTO = new Promise(async (resolve, reject) => {
      await this.productoservices.getProducto(this.idProductoencontrado).subscribe(result => {

        resolve(result);


      }, err => console.log(err));

    });


    await PRODUCTO.then(res => {
      ProductoEncontrado = res;
    })

    let codigoComparar: Number = 0;
    if (ProductoEncontrado === null) {

    } else {
      codigoComparar = Number(ProductoEncontrado.codProducto);
    }

    ///////validar si el codigo es el mismo ha actualizar o si ya existe un codigo en los registros de los porductos
    if (this.idProductoencontrado > 0) {

      if (codigoComparar === Number(this.codigoProductoAnterior)) {
        this.existe = false;

      } else {

        //cambio el valor de la variale para desactivar el boton aceptar si encuentra el producto
        this.existe = true;
      }

    } else {

      this.existe = false;

    }

  }

  OnInitVacio(encontrar: string): void {
    if (encontrar.length == 0) {

    } else {
      this.buscarStockProducto();
      this.existe = false;
    }
    //console.log(encontrar)

  }
}