export interface Parametros{
    idParametros?:number;
    textoBanner?:string;
    mensajePuntosVenta?:string;
    fraseFooter?:string;
    tituloServicios?:string;
    servicio1?:string;
    servicio2?:string;
    servicio3?:string;
    servicio4?:string;
    servicio5?:string;
    tituloInformacion?:string;
    telefono?:string;
    celular?:string;
    correo1?:string;
    correo2?:string;
    direccion?:string;
    urlFotoBanner1?:string;
    urlFotoBanner2?:string;
    urlFotoBanner3?:string;
    idPuntosVentaStock?:string;
    conocenos?:string;
    mision?:string;
    vision?:string;
    
}