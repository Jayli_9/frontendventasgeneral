import { Productos } from './cat_Producto'
import { PuntosVentas } from './cat_PuntosVenta';

export interface cat_stockAuxiliar {
    cantidad?: number;
    catProducto?: Productos;
    catPuntosVenta?: PuntosVentas;
    existe?: string;
    precioDistribuidor?: number;
    precioBulto?: number;
    precioMayor?: number;
    precioUnit?: number;
    stockMax?: number;
    stockMin?: number;

}