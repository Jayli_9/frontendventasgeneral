import { cat_stock } from "./cat_stock";

export interface VenDetalleFact {
    idDetalleFact?: number;
    cantidadFact?: number
    descripcion?: string;
    valorTotal?: number;
    valorTotalSinIva?: number;
    valorUnit?: number;
    valorUnitSinIva?: number;
    ivaProducto?: number;
    catStock?: {
        id: {
            idPuntosVenta?: number;
            idProductos?: number;
        }
    };
}