import { Clientes } from "./Clientes";
import { Usuarios } from "./Usuarios";
import { VenDetalleFact } from "./VenDetalleFact";


export interface VenCabezaFactura {
    idCabezaFac?: number;
    estado?: string;
    tipoPago?: string;
    iva?: number;
    fechaFactu?: string;
    total?: number;
    subtotal?: number;
    descuento?: number;
    tipoComprobante?:string;
    usUser?: Usuarios;
    detallefact?: Array<VenDetalleFact>;
    venCliente?: Clientes;
    secuencial?: string;
    guiaRemision?: string;
    claveAcceso?: string;
    ptoEmision?: string;
    estab?: string;
}