import { Productos } from './cat_Producto'
import { PuntosVentas } from './cat_PuntosVenta';

export interface cat_stock {
    id?: {
        idPuntosVenta?: number;
        idProductos?: number;
    };
    cantidad?: number;
    stockMax?: number;
    stockMin?: number;
    precioDistribuidor?: number;
    precioBulto?: number;
    precioMayor?: number;
    precioUnit?: number;
    existe?: string;

}