export interface Medidas{
    idMedidas?:number;
    abreviacion?:string;
    medida?:string;
    tipo?:string;
}