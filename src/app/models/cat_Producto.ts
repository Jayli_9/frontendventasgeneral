
import {Categorias} from './cat_Categoria'
import {Medidas} from './cat_Medida';
import { Porcentaje } from './cat_porcentaje';

export interface Productos{
    idProductos?:number;
    detalle?:string,
    catCategoria?:Categorias,
    catMedida?:Medidas,
    codProducto?:string,
    urlFoto1?:string,
    urlFoto2?:string,
    urlFoto3?:string,
    valorMedida?:string,
    catPorcentaje?:Porcentaje

}