import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {TokenInterceptorService} from '../services/token-interceptor.service';
//import {AuthInterceptorService} from '../services/auth-interceptor.service';
export const interceptorProviders = 
   [
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true },
    //{ provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true },
];