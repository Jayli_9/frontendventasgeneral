import { Productos } from "./cat_Producto";
import { PuntosVentas } from "./cat_PuntosVenta";

export interface peDetallePedidoAux{
    idDetallePe?: number;
    descripcion?: string;
    valorTotal?: number;
    valorUnit?: number;
    cantidadPe?:number;
    catProducto?: Productos;
    catPuntosVenta?: PuntosVentas;
    
}