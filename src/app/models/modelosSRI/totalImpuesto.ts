export interface totalImpuesto {
    codigo?: string,
    codigoPorcentaje?: string,
    baseImponible?: number,
    tarifa?: number,
    valor?: number
}