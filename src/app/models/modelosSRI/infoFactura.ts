import { pago } from "./pago";
import { totalImpuesto } from "./totalImpuesto";



export interface infoFactura{
        fechaEmision?:string,
        dirEstablecimiento?:string,
        contribuyenteEspecial?:string,
        obligadoContabilidad?:string,
        tipoIdentificacionComprador?:string,
        guiaRemision?:string,
        razonSocialComprador?:string,
        identificacionComprador?:string,
        direccionComprador?:string,
        totalSinImpuestos?:number,
        totalDescuento?:number
        totalImpuesto?:Array<totalImpuesto>,
        propina?:number,
        importeTotal?:number,
        moneda?:string,
        pago?:Array<pago>,
        valorRetIva?:number,
        valorRetRenta?:number,

}