
export interface impuesto {
    codigo?: string,
    codigoPorcentaje?: string,
    tarifa?: number,
    baseImponible?: number,
    valor?: number,
}