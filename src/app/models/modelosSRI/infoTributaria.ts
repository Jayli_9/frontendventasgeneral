export interface infoTributaria{
    ambiente?:string,
    tipoEmision?:string,
    razonSocial?:string,
    nombreComercial?:string,
    ruc?:string,
    codDoc?:string,
    estab?:string,
    ptoEmi?:string,
    secuencial?:string,
    dirMatriz?:string,
}