import { campoAdicional } from "./campoAdicional";
import { detalle } from "./detalle";
import { infoFactura } from "./infoFactura";
import { infoTributaria } from "./infoTributaria";

export interface modeloFacturaSRI{
    id?:string;
    version?:string,
    infoTributaria?:infoTributaria,
    infoFactura?:infoFactura,
    detalle?:Array<detalle>,
    campoAdicional?:Array<campoAdicional>,
    origen?:string,

    
}