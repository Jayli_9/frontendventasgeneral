import { detAdicional } from "./detAdicional";
import { impuesto } from "./impuesto";

export interface detalle {
    codigoPrincipal?:string,
    codigoAuxiliar?:string,
    descripcion?:string,
    cantidad?:number,
    precioUnitario?:number,
    descuento?:number,
    precioTotalSinImpuesto?:number,
    detAdicional?:Array<detAdicional>,
    impuesto?:Array<impuesto>,
    
}