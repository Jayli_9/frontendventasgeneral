import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {PuntosVentas} from '../models/cat_PuntosVenta';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
// import { environment } from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class PuntosVentasService {
  // API_URI ='api/sales-points'
  // URL=environment.url+'api/sales-points';

  constructor( private http: HttpClient) { }

  getPuntosVentas(){
    // return this.http.get(`${this.API_URI}`)
    return this.http.get(`${environment.serviciosBackend}`)
  }
  
  getPuntosVenta(id: number){
    // return this.http.get(`${this.API_URI}/${id}`);
    return this.http.get(`${environment.serviciosBackend}/${id}`);
  }

  savePuntosVentas(puntosventas: PuntosVentas): Observable<PuntosVentas>{
    // return this.http.post(`${this.API_URI}`,puntosventas);
    return this.http.post(`${environment.serviciosBackend}`,puntosventas);

  }
  updatePuntosVentas(id:number, puntosventas: PuntosVentas):Observable<PuntosVentas>{
    // return this.http.put(`${this.API_URI}/${id}`,puntosventas);
    return this.http.put(`${environment.serviciosBackend}/${id}`,puntosventas);
  }

  deletePuntosVentas(id:number){
    // return this.http.delete(`${this.API_URI}/${id}`);
    return this.http.delete(`${environment.serviciosBackend}/${id}`);
  }
}
