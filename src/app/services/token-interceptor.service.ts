import { Injectable} from '@angular/core';
import  {HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, 
  HttpClient,
  HttpErrorResponse} from '@angular/common/http';
import { UsuariosService } from './usuarios.service';
import {CookieService} from "ngx-cookie-service";
import { Observable,throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { UstokenService }  from '../services/ustoken.service';
@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {
  aux : any={
    id:0,
    token: null
     }
     aux_token : any={
      token: null
       }
  constructor(private httpClient:HttpClient,
    private cookieService:CookieService,
    private auth:UstokenService,
    private ruta:Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler):Observable<HttpEvent<unknown>>{
    const token: string = this.cookieService.get('token');
    
    req = req.clone({
      setHeaders:{
        Authorization: `Bearer ${token}`
      }
    })
    
    
    return next.handle(req).pipe(
      catchError((err: HttpErrorResponse) => {

        
        if (err.status === 401) {
          this.ruta.navigate(['/login']);
          this.cookieService.delete('token');
        }

        return throwError(err);

      })
    );

  }
}
