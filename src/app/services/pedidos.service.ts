import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { peCabezaPedido } from '../models/peCabezaPedido';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class PedidosService {
  // API_URI = 'api/order'; 
  // URL=environment.url+'api/order';
  constructor(private http: HttpClient) {   
  }

  getOrder(){
    return this.http.get(`${environment.serviciosBackend}`);
  }
  getOrderbyId(id:number){
    return this.http.get(`${environment.serviciosBackend}/${id}`);
  }
  updateOrderByid(id:number,pedido: peCabezaPedido): Observable<peCabezaPedido>{
    return this.http.put(`${environment.serviciosBackend}/${id}`,pedido);
  }
  saveOrder(pedido: peCabezaPedido): Observable<peCabezaPedido> {
    // return this.http.post(`${this.API_URI}`, pedido);
    return this.http.post(`${environment.serviciosBackend}`, pedido);
  }

  orderreport(idCliente:number,idCabezaPedido:number){
    return this.http.get(`${environment.serviciosBackend}/report/${idCliente}/${idCabezaPedido}`)
  }
  deleteOrder(id:number){
    return this.http.delete(`${environment.serviciosBackend}/${id}`);
  }

  deleteOrder_Detalle(idCabezaPedido:number){
    return this.http.get(`${environment.serviciosBackend}/deletedetalle/${idCabezaPedido}`);
  }
}
