import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Porcentaje }from '../models/cat_porcentaje';
import { environment } from 'src/environments/environment.prod';


@Injectable({
  providedIn: 'root'
})
export class PorcentajesService {

  // URL:string = environment.url+'api/percentage';

  constructor(private http: HttpClient) { }

  getPorcentajes(){
    //return this.http.get(`${this.API_URI}`);
   // console.log("es la Url",environment.serviciosBackend)
    return this.http.get(`${environment.serviciosBackend}`);
 }
 
 getPorcentaje(id:number){
   //return this.http.get(`${this.API_URI}/${id}`);
    return this.http.get(`${environment.serviciosBackend}/${id}`);

 }



 savePorcentaje(porcentaje:Porcentaje):Observable<Porcentaje>{
   //return this.http.post(`${this.API_URI}`,categoria);
   return this.http.post(`${environment.serviciosBackend}`,porcentaje);
 }

 updatePorcentaje(id:number, porcentaje:Porcentaje):Observable<Porcentaje>{
   //return this.http.put(`${this.API_URI}/${id}`,categoria)
   return this.http.put(`${environment.serviciosBackend}/${id}`,porcentaje)
 }

 deletePorcentaje(id:number){
   //return this.http.delete(`${this.API_URI}/${id}`)
   return this.http.delete(`${environment.serviciosBackend}/${id}`)
 }
 
 findbynombre(nombre:string){
  //return this.http.get(`${this.API_URI}/find/${nombre}`);
  return this.http.get(`${environment.serviciosBackend}/find/${nombre}`);
}


}
