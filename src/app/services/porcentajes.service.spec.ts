import { TestBed } from '@angular/core/testing';

import { PorcentajesService } from './porcentajes.service';

describe('PorcentajesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PorcentajesService = TestBed.get(PorcentajesService);
    expect(service).toBeTruthy();
  });
});
