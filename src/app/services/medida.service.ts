import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs'

import { Medidas } from '../models/cat_Medida';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class MedidaService {

  // API_URI = 'api/size';
  // URL=environment.url+'api/size';
  constructor(private http: HttpClient) { }

  getMedidas(){
    // return this.http.get(`${this.API_URI}`);
    return this.http.get(`${environment.serviciosBackend}`);
  }

  getMedida(id: number){
    // return this.http.get(`${this.API_URI}/${id}`);
    return this.http.get(`${environment.serviciosBackend}/${id}`);
  }

  saveMedida(medida: Medidas): Observable<Medidas>{
    // return this.http.post(`${this.API_URI}`,talla)
    return this.http.post(`${environment.serviciosBackend}`,medida)
  }

  updateMedida(id: number, medida: Medidas):Observable<Medidas>{
    // return this.http.put(`${this.API_URI}/${id}`, talla);
    return this.http.put(`${environment.serviciosBackend}/${id}`, medida);
  }

  deleteMedida(id: number){
    // return this.http.delete(`${this.API_URI}/${id}`);
    return this.http.delete(`${environment.serviciosBackend}/${id}`);
  }
  findbynombre(nombre:string){
    // return this.http.get(`${this.API_URI}/find/${nombre}`);
    return this.http.get(`${environment.serviciosBackend}/find/${nombre}`);
  }
}
