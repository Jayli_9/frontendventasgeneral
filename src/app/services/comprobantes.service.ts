import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { modeloFacturaSRI } from '../models/modelosSRI/modeloFacturaSRI';
import { environment } from 'src/environments/environment.prod';
import { resultadoClave } from '../models/modelosSRI/resultadoClave';
@Injectable({
  providedIn: 'root'
})
export class ComprobantesService {
// URL=environment.url+'api/veronica';
  constructor(private http: HttpClient) { }

  crearFacturaSRI(modeloSRI:modeloFacturaSRI):Observable<any>{
    //return this.http.post(`${this.API_URI}`,categoria);
    return this.http.post(`${environment.serviciosBackend}/facturaVeronica`,modeloSRI,{ responseType: 'text' });
  }
  enviarAutorizarFacturaSRI(claveAcceso:string):Observable<any>{
    //return this.http.post(`${this.API_URI}`,categoria);
    return this.http.post(`${environment.serviciosBackend}/autorizarFactura/${claveAcceso}`,null,{ responseType: 'text' });
  }

}
