import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { cat_stock } from '../models/cat_stock';
import { Observable } from 'rxjs';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { environment } from 'src/environments/environment.prod';
@Injectable({
  providedIn: 'root'
})
export class CatStockService {
  //URL = '/api/stock';
  //URL='http://importadorakbackend-env.eba-37sxpzi3.us-east-2.elasticbeanstalk.com/api/stock';
  // URL =environment.url+'api/stock';
  constructor(private http: HttpClient) { }

  getStocks() {
    return this.http.get(`${environment.serviciosBackend}`)
    //return this.http.get(`${this.ulnueva}`)
  }

  getStock(id: number) {
    return this.http.get(`${environment.serviciosBackend}/${id}`);
    //return this.http.get(`${this.ulnueva}/${id}`);
  }

  saveStock(stock: cat_stock): Observable<cat_stock> {
    return this.http.post(`${environment.serviciosBackend}`, stock);

  }
  updateStock(id: number, stock: cat_stock): Observable<cat_stock> {
    return this.http.put(`${environment.serviciosBackend}/${id}`, stock);
    //return this.http.put(`${this.ulnueva}/${id}`, stock);
  }

  getEncontrarStock(idProducto: number, idPuntosVenta: number) {
     return this.http.get(`${environment.serviciosBackend}/number/${idProducto}/${idPuntosVenta}`)
    //return this.http.get(`${this.ulnueva}/number/${idProducto}/${idPuntosVenta}`)

  }

  updateStockCantidadRest(cantidad: number, idProducto: number, idPuntosVenta: number) {
     return this.http.get(`${environment.serviciosBackend}/updateRest/${idProducto}/${idPuntosVenta}/${cantidad}`);
    //return this.http.get(`${this.ulnueva}/updateRest/${idProducto}/${idPuntosVenta}/${cantidad}`);
  }

  findbyIdproductoIdpuntosVenta(id_producto: number, id_puntosventa: number) {
     return this.http.get(`${environment.serviciosBackend}/find/${id_producto}/${id_puntosventa}`);
    //return this.http.get(`${this.ulnueva}/find/${id_producto}/${id_puntosventa}`);

  }
  updateStocks(cantidad: number, precioUnit: number, precioMayor: number, precioDist: number, precioBulto: number, stockMax: number, id_producto: number, stockMin: number, existe:string,id_puntosventa: number) {
     return this.http.get(`${environment.serviciosBackend}/updates/${id_producto}/${id_puntosventa}/${cantidad}/${precioUnit}/${precioMayor}/${precioBulto}/${precioDist}/${stockMax}/${stockMin}/${existe}`);
    //return this.http.get(`${this.ulnueva}/updates/${id_producto}/${id_puntosventa}/${cantidad}/${precioUnit}/${precioMayor}/${precioDist}/${stockMax}/${stockMin}/${existe}`);
  }
  getStockProductbyCodProductoExite(codigoProducto: string, idPuntosVenta: number) {
    return this.http.get(`${environment.serviciosBackend}/product/${codigoProducto}/${idPuntosVenta}`);
    //return this.http.get(`${this.ulnueva}/product/${codigoProducto}/${idPuntosVenta}`);
  }

  getAllStockExistents(inicio: number, numeroFilas: number) {
     return this.http.get(`${environment.serviciosBackend}/exist/${inicio}/${numeroFilas}`);
    //return this.http.get(`${this.ulnueva}/exist/${inicio}/${numeroFilas}`);
  }
  getStockAllExistPuntoVenta(id:number,inicio: number, numeroFilas: number){
     return this.http.get(`${environment.serviciosBackend}/exist/${id}/${inicio}/${numeroFilas}`);
    //return this.http.get(`${this.ulnueva}/exist/${id}/${inicio}/${numeroFilas}`);
  }
  getCantExistents() {
    
     return this.http.get(`${environment.serviciosBackend}/cant`);
    //return this.http.get(`${this.ulnueva}/cant`);
  }
  findStockInventario(){
     return this.http.get(`${environment.serviciosBackend}/findInventario`);
    //return this.http.get(`${this.ulnueva}/findInventario`);
  }
  findStockInventarioPuntoVenta(id:number){
     return this.http.get(`${environment.serviciosBackend}/findInventario/${id}`);
    //return this.http.get(`${this.ulnueva}/findInventario/${id}`);
  }
  findStockbyMin(){
    return this.http.get(`${environment.serviciosBackend}/findMin`);
    //return this.http.get(`${this.ulnueva}/findMin`);
  }
  findStockbyMinPuntoVenta(id:number){
    return this.http.get(`${environment.serviciosBackend}/findMin/${id}`)
    //return this.http.get(`${this.ulnueva}/findMin/${id}`)
  }
  findStockbyParameters(parametros:string){
    return this.http.get(`${environment.serviciosBackend}/findparameteros/${parametros}`)
    //return this.http.get(`${this.ulnueva}/findparameteros/${parametros}`)
  }
  findStockbyParametersPuntoVenta(id:number,parametros:string){
    
    
     return this.http.get(`${environment.serviciosBackend}/findparameterospuntosventa/${parametros}/${id}`)
    //return this.http.get(`${this.ulnueva}/findparameterospuntosventa/${parametros}/${id}`)
  }

  reporteStockTotal(){
  
   return this.http.get(`${environment.serviciosBackend}/reportTotal`)
  }
  reporteStockTotalLocal(idpuntoventa:number){
    
    return this.http.get(`${environment.serviciosBackend}/report/${idpuntoventa}`)
   }

   reporteStockMinTotalLocal(idpuntoventa:number){
    
    return this.http.get(`${environment.serviciosBackend}/report/minTotalPoints/${idpuntoventa}`)
   }
   reporteStockMinTotal(){
    
    return this.http.get(`${environment.serviciosBackend}/report/minTotal`)
   }
   reporteCodigoBarra(){
    
    return this.http.get(`${environment.serviciosBackend}/codigoBarra`)
   }
}
