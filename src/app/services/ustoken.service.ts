import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';


import {UsToken} from '../models/UsToken';
import { environment } from 'src/environments/environment.prod';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class UstokenService {
  // URL=environment.serviciosBackend+'api/token';

  constructor(private http: HttpClient) { }

  getTokenUsuario(){
    // return this.http.get(`${this.API_URI}/${id}`);
    return this.http.get(`${environment.serviciosBackend}/1`);
    
  }
  
  getTokenUsuarioVeronica(){
    // return this.http.get(`${this.API_URI}/${id}`);
    return this.http.get(`${environment.serviciosBackend}/2`);

  }


  updateUsuarioFYL(token:UsToken):Observable<UsToken>{
    // return this.http.put(`${this.API_URI}/${id}`,usuarios);
    return this.http.put(`${environment.serviciosBackend}/1`,token);
  }

  updateUsuarioVeronica(token:UsToken):Observable<UsToken>{
    // return this.http.put(`${this.API_URI}/${id}`,usuarios);
    return this.http.put(`${environment.serviciosBackend}/2`,token);
  }

}
