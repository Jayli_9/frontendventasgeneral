import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'

import { VenCabezaFactura } from '../models/VenCabezaFactura'
import { VenDetalleFact } from '../models/VenDetalleFact'
import { environment } from 'src/environments/environment.prod';


@Injectable({
  providedIn: 'root'
})
export class FacturacionService {

  constructor(private http: HttpClient) { }

  // API_URI = 'api/bill'; 
  // URL = environment.url + 'api/bill';

  saveFactura(factura: VenCabezaFactura): Observable<VenCabezaFactura> {
    // return this.http.post(`${this.API_URI}`, factura);
    return this.http.post(`${environment.serviciosBackend}`, factura);
  }
  facturafechas(fechainicio: string, fechafin: string) {
    // return this.http.get(`${this.API_URI}/dates/${fechainicio}/${fechafin}`);
    
    return this.http.get(`${environment.serviciosBackend}/dates/${fechainicio}/${fechafin}`);
  }
  consultaSecuencial() {

    return this.http.get(`${environment.serviciosBackend}/secuencial/`);
  }
  facturafechasTipoPago(fechainicio: string, fechafin: string, tipoPago: string) {
    // return this.http.get(`${this.API_URI}/dates/${fechainicio}/${fechafin}`);
    
    return this.http.get(`${environment.serviciosBackend}/dates/${fechainicio}/${fechafin}/${tipoPago}`);
  }
  facturafechasLocales(fechainicio: string, fechafin: string, idPuntosVenta: number) {
    // return this.http.get(`${this.API_URI}/dates/${fechainicio}/${fechafin}`);
    return this.http.get(`${environment.serviciosBackend}/datesLocal/${fechainicio}/${fechafin}/${idPuntosVenta}`);
  }
  facturafechasLocalesTipoPago(fechainicio: string, fechafin: string, idPuntosVenta: number, tipoPago) {
    // return this.http.get(`${this.API_URI}/dates/${fechainicio}/${fechafin}`);
    return this.http.get(`${environment.serviciosBackend}/datesLocal/${fechainicio}/${fechafin}/${idPuntosVenta}/${tipoPago}`);
  }
  reporteFacturaFechas(fechaDesde: string, fechaHasta: string, totalVentas: string) {
    return this.http.get(`${environment.serviciosBackend}/reporteFecha/${fechaDesde}/${fechaHasta}/${totalVentas}`)
  }
  reporteFacturaFechasTipoPago(fechaDesde: string, fechaHasta: string, totalVentas: string, tipoPago: string) {
    return this.http.get(`${environment.serviciosBackend}/reporteFecha/${fechaDesde}/${fechaHasta}/${totalVentas}/${tipoPago}`)
  }
  reporteFacturaFechasLocal(fechaDesde: string, fechaHasta: string, totalVentas: string, idPuntosVenta: number) {
    return this.http.get(`${environment.serviciosBackend}/reporteFechaLocal/${fechaDesde}/${fechaHasta}/${totalVentas}/${idPuntosVenta}`)
  }
  reporteFacturaFechasLocalTipoPago(fechaDesde: string, fechaHasta: string, totalVentas: string, idPuntosVenta: number, tipoPago: string) {
    return this.http.get(`${environment.serviciosBackend}/reporteFechaLocal/${fechaDesde}/${fechaHasta}/${totalVentas}/${idPuntosVenta}/${tipoPago}`)
  }
  ticket(idfactura: number) {
    return this.http.get(`${environment.serviciosBackend}/ticket/${idfactura}`)
  }
  reporteFcturaFechasLocal(fechaDesde: string, fechaHasta: string, idPuntosVenta: number) {
    return this.http.get(`${environment.serviciosBackend}/reporteFechaLocal/${fechaDesde}/${fechaHasta}/${idPuntosVenta}`);
  }

  facturaId(idfactura: number) {
    return this.http.get(`${environment.serviciosBackend}/${idfactura}`)
  }

  updateFactura(idfactura: number, factura: VenCabezaFactura): Observable<VenCabezaFactura> {
    return this.http.put(`${environment.serviciosBackend}/${idfactura}`, factura)
  }

}
