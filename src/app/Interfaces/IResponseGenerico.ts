export interface IResponseGenerico<T>{
    codigoRespuesta: string;
    mensaje: string;
    objeto: T;
    listado: Array<T>;
    totalRegistros: number;
    totalPaginas:number;
}